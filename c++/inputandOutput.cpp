/*
                                                Input and Output (Entrada y salida)
DOCUMENTACION:

En C ++, puede leer un solo símbolo de entrada de espacio en blanco, utilizando cin, y imprimir salida en stdout
mediante cout. Por ejemplo, supongamos que declaramos las siguientes variables:

string s;
int n;
Y queremos usar cin para leer la entrada "High 5" de stdin. Podemos hacer esto con el siguiente código:

cin >> s >> n;

El código anterior lee la primera palabra ("High") de stdin y la guarda como cadena s, luego lee la segunda palabra ("5")
de stdin y la guarda como número entero n. 
Si queremos imprimir estos valores en stdout, escribiremos el código siguiente:

cout << s << " " << n << endl;

El código anterior imprime el contenido de la cadena s, que es la palabra "High".
A continuación, imprime un único espacio (" "), seguido por el contenido del entero n.
Debido a que también queremos asegurarnos de que no se imprima nada más en esta línea,
finalizamos nuestra línea de salida con una nueva línea vía endl. Esto da como resultado el resultado siguiente:

High 5

TAREA

Lea 3 números de stdin e imprima su suma a stdout.

Nota
Si planea completar este desafío en C en lugar de C ++
necesitará utilizar especificadores de formato con printf y scanf.

FORMATO DE ENTRADA

Una sola línea que contiene 3 enteros separados por el espacio: a, b, y c.

FORMATO DE SALIDA

Imprima la suma de los tres números en una sola línea.
*/
// Declaracion de librerias ojo a diferencia de c las librerias no llevan el .h
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
// No se que es.
using namespace std;

// Funcion Principal.
int main() {
   // Declaracion de variables de tipo entero, a, b, c.
   int a,b,c;
   // Ingresando dato a,b,c.
   cin >> a >> b >> c;
   // Imprimiendo la suma de a,b,c.
   cout << a + b + c;
   
   // Terminar la ejecucion del programa con exito.
   return 0;
}
