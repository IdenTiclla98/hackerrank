/*
                                          Basic Data Types (Tipos de datos básicos)
DOCUMENTACION

Algunos tipos de datos C ++, sus especificadores de formato y sus anchos de bits más comunes son los siguientes:

   Int ("%d"): 32 Bit integer.
   Long ("%ld"): 64 bit integer.
   Char ("%c"): Character type.
   Float ("%f"): 32 bit real value.
   Double ("%lf"): 64 bit real value.

NOTA
   Tambien podemos usar cin y cout en lugar de scanf y printf.
   Sin embargo cuando trabajamos con millones de datos y millones de impresiones es mas rapido usar scanf y printf
FORMATO DE ENTRADA
   La entrada consta de los sgtes valores separados por espacio: int,long,char,float, double.
FORMATO DE SALIDA
   Imprima cada dato en una nueva linea en el mismo orden que se recibio los daots ojo para la precision del flotante
   deben ser de 3 decimales para el double son 9 decimales.


*/
// Declaracion de librerias.
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
//
using namespace std;
// Funcion Principal.
int main(){
   // Declarando variable entera a.
   int a;
   // Declarando variable entera de 64 bits.
   long b;
   // Declarando variable de tipo caracter.
   char c;
   // Declarando variable de tipo real.
   float f;
   // Declarando variable de tipo real con mas precision.
   double d;
   // Ingresando dato a.
   cin >> a;
   // Ingresando dato b.
   cin >> b;
   // Ingresando dato c.
   cin >> c;
   // Ingresando dato f.
   cin >> f;
   // Ingresando dato d.
   cin >> d;

   
   // Imprimiendo datos a,b,c,f,d uno por linea.
   cout << a << endl;
   cout << b << endl;
   cout << c;
   cout << endl;
   printf("%.3f",f);
   cout << endl;
   printf("%.9f",d);
   cout << endl;
   // Terminando ejecucion con exito.
   return 0ñ
}