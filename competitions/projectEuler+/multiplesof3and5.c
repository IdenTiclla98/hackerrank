/*
    DOCUMENTACION:
    
    Si enumeramos todos los números naturales por debajo de 10 que son múltiplos de 3 o 5 obtenemos:
    3,5,6 y 9 la suma de estos múltiplos es 23
    Encontrar la suma de todos los múltiplos de 3 o 5 por debajo de n.
    
    FORMATO DE ENTRADA
    
    * Primera línea contiene T que denota el número de casos de prueba.
    * Esto es seguido por líneas T, cada una conteniendo un entero N.
    
    FORMATO DE SALIDA
    
    * Para cada caso de prueba, imprima un entero que denota la suma de todos los múltiplos de 3 o 5 por debajo de n
*/
// Declaracion de librerias
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
// Funcion principal.
int main()
{
    // Declaracion de variable entera t: denota casos de prueba.
    long long t; 
    // Ingresando dato t.
    scanf("%lld",&t);
    long long n;
    
    for(int i = 0; i < t; i++)
    {
        scanf("%lld",&n);
        long long cantmult3 = (n - 1) / 3;
        long long cantmult5 = (n - 1) / 5;
        long long cantmult3y5 = (n - 1) / 15;
        long long resultado = 3*cantmult3*(cantmult3 + 1) / 2 + 5*cantmult5*(cantmult5 + 1) / 2 - 15*cantmult3y5*(cantmult3y5 + 1) / 2;
        printf("%lld\n",resultado);
    }
    // Terminar ejecucion con exito.
    return 0;
}