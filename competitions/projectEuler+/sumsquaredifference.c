/*
   DOCUMENTACION:
   
   La suma de los cuadrados de los diez primeros números naturales es 1 ^ 2 + 2 ^ 2 ... + 10 ^ 2 = 385.
   El cuadrado de la suma de los diez primeros números naturales es, (1 + 2 + ... + 10) ^ 2 = 55 ^ 2 = 3025.
   Por lo tanto, la diferencia absoluta entre la suma de los cuadrados de los diez primeros números naturales y el cuadrado de la suma es:
   3025-385 = 2640.

   Encuentra la diferencia absoluta entre la suma de los cuadrados de los primeros N números naturales y el cuadrado de la suma.
   
   
   FORMATO DE ENTRADA
   
   * La primera línea contiene T que denota el número de casos de prueba. A esto le siguen las líneas T, cada una con un número entero, N.

   FORMATO DE SALIDA

   * Imprima la respuesta necesaria para cada caso de prueba.
*/
// Declaracion de librerias.
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
// Declaracion de funcion principal.
int main()
{
    // Declaracion de variable entera t: denota el numero de casos.
    int t; 
    // Ingresando t.
    scanf("%d",&t);
    
    // Iterando t veces.
    for(int i = 0; i < t; i++)
    {
        // Declaracion de variable entera n.
        long long int n; 
        // Ingresando n.
        scanf("%lld",&n);
        // Declaracion variable sumaCuadrada (1+2+3+4+5..+n)^2.
        long long int sumaCuadrada = ((n + 1) * n) / 2;
        sumaCuadrada = pow(sumaCuadrada,2);
        // Declaracion sumad de cuadrados (1^2+2^2+3^3....+n^2).
        long long int sumadeCuadrados = (n*(n + 1)*(2*n + 1)) / 6;
        // Imprimiendo diferencia absoluta entre sumaCuadrada - suma de cuadrados.
        printf("%lld\n",llabs(sumaCuadrada-sumadeCuadrados));
    }
    // Terminar ejecucion con exito.
    return 0;
}