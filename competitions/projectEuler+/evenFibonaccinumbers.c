/*
                                             Even fibonacii numbers
DOCUMENTACION:

Cada nuevo término en la secuencia de Fibonacci se genera sumando los dos términos anteriores.
Comenzando con 1 y 2, los primeros 10 términos serán: 1,2,3,5,8,13,21,34,55,89 ...

Considerando los términos en la secuencia de Fibonacci cuyos valores no exceden N, encuentre la suma de los términos pares.

FORMATO DE ENTRADA:

La primera línea contiene T que denota el número de casos de prueba.
A esto le siguen las líneas T, cada una con un número entero, N.

FORMATO DE SALIDA:

Imprima la respuesta necesaria para cada caso de prueba.
*/


// Declaracion de librerias.
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
// Funcion principal.
int main()
{
    // Declaracion de variable entera t: que denota el numero de casos.
    int t; 
    // Ingresando dato t.
    scanf("%d",&t);
    // Iterando t veces.
    for(int i = 0; i < t; i++)
    {
        // Declaracion n variable entera long.
        long n; 
        // Ingresando n.
        scanf("%ld",&n);
        
        printf("%ld\n",n);
        
    }
    // Terminando ejecucion con exito
    return 0;
}
