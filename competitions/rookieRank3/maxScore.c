/*
                                                   Max Score
   DOCUMENTACION:
   
   Consideremos un n-elemento de enteros, A = {a0, a1, a2, ..., a (n-1)} queremos realizar n operaciones en A donde:
   cada operación se define por la siguiente secuencia de pasos:
   
   1. Quite cualquier entero, ai, de A y déjelo a un lado.
   2. caculate score_k = runningSum mod a_i donde 1 <= k <= n y runningSum es la suma de todos los números eliminados de A durante las operaciones k-1 anteriores.
   3. update runningSum tal que runningSum = runningSum + a_i, donde a_i es el entero que fue eliminado de A durante el paso 1 anterior.
   
   Después de realizar una operación, sumamos cada puntaje para obtener la puntuación total. En otras palabras:
   Totalscore es igual a la sumatoria todos los puntajes
   
   Dado ny A, busque e imprima el máximo valor posible del puntaje total después de realizar n operaciones\
   
   NOTA:
   
   * los valores iniciales de runningSum y score_i siempre son 0
   
   FORMATO DE ENTRADA

   * La primera línea contiene un número entero, n, que indica el número de elementos en la secuencia.
   * La segunda línea contiene n enteros separados por el espacio que describen los respectivos valores de a0, a1, a2 ... a (n-1)
   
   FORMATO DE SALIDA
   * Imprima un número entero que indique el valor máximo posible del puntaje total después de realizar n operaciones.
*/

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>

long getMaxScore(int a_size, long *a);


int main() 
{
    // Declaracion de variable entera n      
    int n; 
    // Ingresando dato n
    scanf("%d", &n);
    // Declaracion de variable a vector tipo long con asignacion dinamica para n valores long 
    long *a = malloc(sizeof(long) * n);
    // Iterando la variable n desde a_i = 0 hasta n - 1 
    for(int a_i = 0; a_i < n; a_i++)
    {
       // Ingresando dato a[a_i]
       scanf("%ld",&a[a_i]);
    }
    
    long maxScore = getMaxScore(n,a);
    // Imprimiendo maxScore
    printf("%ld\n", maxScore);
    // Retornar 0.
    return 0;
}

long getMaxScore(int a_size, long *a)
{
    // Complete this function
    int runningSum = 0;
    long score = 0;
    for(int i = a_size - 1; i >= 0;i--)
    {
       
       int element = a[i];
       score = score + (runningSum  % element);
       runningSum = runningSum + element;
       
    }
    return score;
}