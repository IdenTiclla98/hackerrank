/*
                                          Calculate Array Size (Calcular el tamaño del arreglo)
DOCUMENTACION:

Definimos una matriz multidimensional de enteros para ser una matriz que contiene una o más matrices de enteros.
Estos son algunos ejemplos de arrays multidimensionales declarados en C:

int a[93401];      // 1-dimensional array
int b[10][1000];   // 2-dimensional array
int c[25][30][70]; // 3-dimensional array
int d[15][48];     // 2-dimensional array

Queremos encontrar la cantidad de espacio de almacenamiento requerido por la matriz en kilobytes,
suponiendo que:
1 entero equivale a 4 bytes.
1024 bytes equivalen a 1 kilobyte.

Por ejemplo, consideremos la matriz b[10][1000], que es una matriz de números enteros bidimensionales
con espacio para 10x1000 = 10000 enteros, lo que requiere 10000x4 = 40000 bytes de espacio.
Cuando convertimos nuestros bytes en kilobytes, obtenemos (40000/1024) = 39.0625 kilobytes.

Dado los tamaños respectivos para cada dimensión de una matriz n-dimensional.
imprima el espacio de almacenamiento de la matriz en kilobytes como un solo entero redondeado al piso.

FORMATO DE ENTRADA.

La primera línea contiene un número entero denotando n (el número de dimensiones en la matriz multidimensional).
La segunda línea contiene n enteros separados por espacio que describen los respectivos valores de d1, d2, .., dn.
para un arreglo multidimensional con las dimensiones [d1] [d2] .. [dn].

FORMATO DE SALIDA.
Imprime un entero redondeado en el suelo que indica el espacio de almacenamiento de los arrays multidimensionales en kilobytes.
*/
// Declaracion de librerias
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
// Prototipos de funciones.
int getArrayKb(int n, int* d);
// Declaracion de funcion principal.
int main(){
   
//  Return the size of the multidimensional array in kilobytes. Return only the integer part.(Solo retornar parte entera)

// Declaracion de variable n:denota el numero de dimensiones de la matriz.
int n; 
// Ingresando dato n.
scanf("%d", &n);
// Declaracion de puntero a entero con memoria dinamica para almacenar n enteros.
int *d = malloc(sizeof(int) * n);
//Iterando la dimension.
for(int i = 0; i < n; i++){
 scanf("%d",&d[i]);
}
// Declaracion de variable resultado que obtendra el valor retornado de la funcion getArray que obtiene dos parametros.
int result = getArrayKb(n, d);
printf("%d\n", result);
// Liberando memoria dinamica prestada.
free(d);
// Terminando ejecucion con exito.
return 0;
}
// Definicion de funciones
// Funcion getArrayKb recibe 2 parametros:(entero,puntero a entero).
int getArrayKb(int n, int* d){
   // Complete this function
   int size = 1;
   // Iterando dimensiones
   for(int i = 0; i < n; i++){
      // Guardar en size la multiplicacion de todas las dimensiones.
      size *= d[i];
   }
   // Multiplicamos por 4 el tamaño total para saber el tamaño en bytes.
   size *= 4;
   // Retornamos el tamañon dividido entre 1024 para saber cuntos kilobytes tenemos.
   return size/1024;
}