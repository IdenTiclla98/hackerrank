/*
                                             Cartesian Country (País cartesiano)
DOCUMENTACION:

Cartesian Country es una región rectangular del plano xy con esquinas en (x1, y1) y (x2, y2).
Cada punto de celosía (es decir, punto con coordenadas x e y enteras) 
en País Cartesiano denota una ciudad insular rodeada de agua.

Los gobernantes del país viven en la capital en coordenadas (xc, yc).
Viajar entre las ciudades y la capital es un traicionero viaje marítimo,
por lo que deciden poner puentes que cumplan todas las condiciones siguientes:

* Un puente es una línea recta con puntos finales en dos ciudades no capitales.
* El capital debe estar en el centro exacto (o punto medio) de la línea.
* Dos puentes superpuestos se consideran diferentes si conectan ciudades diferentes.

Por ejemplo:
el diagrama de la izquierda representa País cartesiano como un rectángulo con esquinas opuestas en (1,1) y (5,4)
y ciudades aisladas en sus puntos de reticulación.
El diagrama de la derecha representa el número máximo de puentes
que podemos construir cuando el capital se encuentra en (2,3) (es decir, el círculo rojo):
Observe que las dos ciudades conectadas por un puente tienen el mismo color,
y hemos construido un máximo de 4 puentes entre las ciudades (1,4) y (3,2), (1,3) y (3,3), ( 1,2) y (3,4), (2,2) y (2,4).

Dado, (x1, y1), (x2, y2) y (xc, yc), encuentre e imprima el número máximo de puentes que las Reglas comisionarán.

FORMATO DE ENTRADA

* La primera línea contiene dos enteros separados por espacio que describen los respectivos valores de x1 e y1.
* La segunda línea contiene dos enteros separados por el espacio que describen los respectivos valores de x2 e y2.
* La tercera línea contiene dos enteros separados por el espacio que describen los respectivos valores de xc e yc.

FORMATO DE SALIDA
* Imprima un entero largo que indique el número máximo de puentes que las Reglas emitirán.
*/
// Declaracion de librerias.
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
// Prototipos de funciones.
long getMaxBridges(long x1, long y1, long x2, long y2, long xC, long yC);

// Funcion principal.
int main(){
//  Return the maximum number of bridges the Rulers will commission.
// Declaracion de variables x1, y2 que seran el punto de la ezquina inferior izquierda de tipo long.
long x1,y1; 
// Ingresando datos x1, y1.
scanf("%ld %ld", &x1, &y1);
// Declaracion de variables x2, y2 que seran el punto de la ezquina superior derercha de tipo long.
long x2,y2; 
// Ingresando datos x2,y2.
scanf("%ld %ld", &x2, &y2);
// Declaracion de variables xC, YC que seran el punto medio de tipo long.
long xC,yC; 
// Ingresando datos xC, yC.
scanf("%ld %ld", &xC, &yC);
// Resultado sera igual al resultado de la funcion getMaxBridges: que recibe 6 parametros()
long result = getMaxBridges(x1, y1, x2, y2, xC, yC);
// Imprimiendo resultado.
printf("%ld\n", result);
//Terminando ejecucion con exito.
return 0;
}
// Definicion de funciones.
long getMaxBridges(long x1, long y1, long x2, long y2, long xC, long yC){
// Complete this function
long alturaMaxima = y2;
long anchoMaximo = x2
long xMedia = xC;
long yMedia = xY;

}
