/*
                                                Numeric String (Cadena numérica).
DOCUMENTACION:
Alexa realiza la siguiente secuencia de operaciones en una cadena, s, de enteros:
1 Haga una lista de todas las subcadenas de s que tienen una longitud dada, k.
2 Considere que cada subcadena sea un número base b (donde se da b).
  Convertir cada subcadena en la lista de la base b a la base 10
  (es decir, convertir (subcadena) b -----> (subcadena) 10), sustituyendo los números base b por los números base-10.
3 Reemplace cada número base 10 por su valor modulo a m dado (es decir, (x) 10 ---> (x) 10 mod m).
4 Calcule el número mágico sumando todos los enteros en la lista final.

FORMATO DE ENTRADA:
* La primera línea contiene una cadena que indica s.

* La segunda línea contiene tres enteros separados por el espacio que describen los respectivos valores de:
  k (la longitud de la subcadena), b (la base) y m (el módulo).
La cadena s se compone de dígitos decimales en el rango inclusivo [0, b - 1]
por lo que cada subcadena de longitud k siempre forma un número base b válido.

La cadena s puede contener ceros a la izquierda.

FORMATO DE SALIDA:

Imprima un número entero que indique el número mágico.

*/

// DECLARACION DE LIBRERIAS.
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

// PROTOTIPOS DE FUNCIONES.
long long convertBase10(char* string,int baseInicial);
long long getMagicNumber(char* s, int k, int b, int m);
char* optimizar(char* cadena);

// FUNCION PRINCIPAL.
int main(){
    char buffer_aux [3000001];
    // Declaracion de variable s de tipo puntero a caracter que tendra memoria dinamica para 512000 caracteres.
    // Ingresando dato s.
    scanf("%s", buffer_aux);
    
    char* s = (char *)malloc(strlen(buffer_aux) * sizeof(char) + 1);
    strcpy(s,buffer_aux);

    // Delcaracion de entero k: que denota la longitud de la subcadena.
    int k; 
    // Declaracion de entero b: que denota la base.
    int b; 
    // Declaracion de entero m: que denota el modulo.
    int m; 
    // Ingresando datos k,b,m.
    scanf("%d %d %d", &k, &b, &m);
    /* Declaracion de result de tipo entero que obtendra el valor que retorna la funcion getMagicNumber.
    getMagicNumber Recibe como parametro 4 parametros(puntero a caracter,entero,entero,entero)*/
    s = optimizar(s);
    long long result = getMagicNumber(s, k, b, m);
    // Imprimiento el resultado.
    printf("%lld\n", result);
    // Liberando memoria dinamica.
    free(s);
    // Terminando ejecucion del programa con exito.
    return 0;
}
// DEFINICION DE FUNCIONES.

long long getMagicNumber(char* s, int k, int b, int m){
    // Declaracion de variable buffer de tipo caracter que almacenara k caracteres + 1 para caracter fin de cadena.
    char buffer[k + 1];
    buffer[k] = '\0';
    // Variable inicio tipo entero igual a 0.
    int inicio = 0; 
    // Calculado longitud de la cadena s.
    int longitud = strlen(s);
    // Variable entera Sumatoria iniciada en 0.
    long long sumatoria = 0;
    // Bucle While (Mientras) k < longitud.
    while(k <= longitud){
       // Declaracion de puntero a caracter substring que tendra memoria dinamica para k caracteres.    
       char* substring = malloc(sizeof(char)*k + 1);
       int j = 0;
       // Iterando k veces:
       for(int i = inicio; i < k; i++)
       {
          // Caracter j-esimo de buffer igual al caracter iesimo de la cadena.
          buffer[j] = s[i];
          // Incrementamos j.
          j++;
       }
       // Copiando a substring  lo que tenemos en el buffer.
       strcpy(substring,buffer);
       // base 10 obtiene el resultado de la funcion convertir a base 10
       long long base10 = convertBase10(substring,b);
       // Sumatoria se incrementa en base10 % m.
       sumatoria += base10 % m;
       // Inicio aumenta.
       inicio++;
       // k aumenta.
       k++;
    }   
    // Retornamos Sumatoria.
    return sumatoria;
}

long long convertBase10(char* string,int baseInicial){
   // Declaracion de variable longitud que sera el indice final.
   int longitud = strlen(string) - 1;
   // Declaracion de variable entera acumulador inicia en 0.
   long long acumulador = 0;
   // Varibable entera j inicia en 0.
   int j = 0;
   // Desde el ultimo indice hasta el primer indice 0.
   for(int i = longitud; i >= 0; i--){
      char ch = string[i];
      acumulador += pow(baseInicial,j)*atoi(&ch);
      j++;
   }
   // Retorna acumulador.
   return acumulador;
}

// Optimizar


char* optimizar(char* cadena){
   int longitud = strlen(cadena);
   char buffer[longitud];
   int indice;
   

   for(int i = 0; i < longitud; i++){
      if(cadena[i]!='0'){
         indice = i;
         i = strlen(cadena);
      }
   }   
   int j = 0;
   for(int i = indice; i < strlen(cadena);i++)
   {
      buffer[j] = cadena[i];
      j++;
   }
   
   
   char* result = malloc(sizeof(char)*strlen(buffer));
   strcpy(result,buffer);
   return result;
}
