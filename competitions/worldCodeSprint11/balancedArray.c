/*
                                                   Balanced Array (Arreglo Balanceado).
DOCUMENTACION:
Emma tiene una matriz [a0, a1, a2 ....., a (n-1)] de tamaño n en la que n es un número par.
Una matriz es equilibrada si:
la suma de la mitad izquierda de los elementos de la matriz es igual a la suma de la mitad derecha.
Para equilibrar una matriz, Emma puede agregar un entero x no negativo (x> = 0)
a cualquier elemento de la matriz ai. Su tarea es encontrar el valor más pequeño de x que hace que la matriz equilibrada.

FORMATO DE ENTRADA.
* La primera línea contiene un número entero n.

* La segunda línea contiene los n elementos enteros de la matriz [a0, a1, a2, ..., a(n-1)].


FORMATO DE SALIDA.

* Imprima el valor mínimo de x en una sola línea.


*/
// Declaracion de librerias.
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
//Prototipos de funciones
bool esPar(int num);
bool rangoN(int n);
bool rangoElement(int element);
int solve(int a_size, int* a);
// Funcion Principal.
int main(){
    // Declaracion de variable tipo entero n: que denota la dimension del arreglo.
    int n; 
    // Bucle do-while (Hacer mientras)
    do
    {
      // Ingresando dato n.
      scanf("%d", &n);             
    }while(!esPar(n) || !rangoN(n));
    // Declaracion de variable a tipo puntero a entero con memoria dinamica para almacenar n enteros. 
    int *a = malloc(sizeof(int) * n);
    // Iterando n veces. Desde i = 0 hasta i = n - 1.
    for(int i = 0; i < n; i++){
       // Bucle do-while (Hacer mientras).
       do
       {
          // Ingresando dato iesimo en el arreglo.
          scanf("%d",&a[i]);
       }while(!rangoElement(a[i]));
    }
    /*Declaracion de variable result de tipo entero que almacenara el resultado de la funcion solve:
    solve: recibe 2 enteros como parametros y retorna un entero.*/
    int result = solve(n, a);
    // Imprimiendo el resultado.
    printf("%d\n", result);
    // Liberando memoria dinamica.
    free(a);
    // Terminando ejecucion.
    return 0;
}
// Definicion de funciones.
bool esPar(int num){
   if(num % 2 == 0){
      return true;
   }
   return false;
}
bool rangoN(int n){
   if(n >= 1 && n <= 100){
      return true;
   }
   return false;
}
bool rangoElement(int element){
   if(element >= 0 && element <= 100){
      return true;
   }
   return false;
}
int solve(int size, int* a){
    // Complete this function
    int middle = (size - 1) / 2;
    int sumIzq = 0;
    int sumDer = 0;
    for(int i = 0; i <= middle; i++)
    {
       sumIzq += a[i];
    }
    for(int i = middle + 1; i < size; i++)
    {
       sumDer += a[i];
    }
    return abs(sumIzq - sumDer);
}