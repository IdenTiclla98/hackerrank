/*
                                                Duplication
   DOCUMENTACION:
   
   1. Crear una cadena, T donde cada carácter t [i] es igual a 1 - s [i] Por ejemplo, si s = "01", entonces t = "10".
      Tenga en cuenta que t y s siempre tienen la misma longitud porque t es el complemento de s

   2. Añada t al final de s para que s_expanded = s_initial + t_sinitial. En el ejemplo anterior, s se convierte en "0110".
   
   3. Seguimos expandiendo s usando los pasos 1 y 2 hasta que la longitud de s excede 1000.
   
   Cuando repetimos la operación de expansión, la cadena s crece así


   FORMATO DE ENTRADA:
   * La primera línea contiene un entero que indica un (número de consultas)
   * Cada una de las q siguientes líneas contiene un entero que describe el valor de x para una consulta
   
   FORMATO DE SALIDA:
   
   * Para cada consulta, imprima el valor de s [x] (es decir, 0 o 1) en una nueva línea.
   
*/
// Declaracion de librerias.
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <limits.h>
#include <stdbool.h>
// Prototipos de funciones.
char* duplication(int x);
char* complement(char* cadena);
void concatenar(char* concatenacion, char* fuente0, char* fuente1);
// Funcion Principal.
int main()
{
    // Declaracion de variable  q: numero de consultas.
    int q;
    // Obteniendo numero de consultas.
    scanf("%d", &q);
    if(!((q >= 0) && (q <= 1000)))
    {
       return 1;
    }
    // variable result tipo puntero a caracter que recibe el resultado de duplication(x).
    char* result = duplication(q);
    
    char resultados[q];
    // Iterando q veces.
    for(int i = 0; i < q; i++)
    {
        // Dato x:indice.
        int x;
        // Ingresando dato x.
        scanf("%d", &x);
        if(!((x >= 0) && (x <= 1000)))
        {
            return 1;
        }

        // El valor iesimo de resultados sera el caracter de result con indice x.
        resultados[i] = result[x];
    }
    // Iterando q veces.
    for(int i = 0; i < q; i++)
    {
        // Imprimiendo el caracter iesimo de resultados.
        printf("%c\n",resultados[i]);
    }
    // Terminar ejecucion con exito.
    return 0;
}
// Definicion de funciones.
// Funcion duplication devuelve un puntero a caracter y recibe como parametro un entero q.
char* duplication(int q)
{
   // Inicializando s inicial a 0
   char* s_ini = "0";
   char* t = malloc(sizeof(s_ini));
   char* s_expa = malloc(sizeof(t)*2);
   // Iteramos q veces.
   for(int i = 0; i <= q; i++)
   {
         // Sacando el complemento de s y guardando en t.
         t = complement(s_ini);
         // Asignando memoria a s_expa
         s_expa = malloc(sizeof(char)*(strlen(s_ini)+(strlen(t))));
         // Concatenando en s_expa los valore de s_ini + t.
         concatenar(s_expa,s_ini,t);
         // s_ini toma el tamaño de s_expa.
         s_ini = malloc(sizeof(s_expa));
         s_ini = s_expa;
   }
   // Retorna s expandida.
   return s_expa;
}

// Funcion concatenar devuelve nada y recibe 3 parametros punteros a caracteres fuente0 y fuente1 se concatenan en concatenacion.
void concatenar(char* concatenacion, char* fuente0, char* fuente1)
{
   int i = 0;
   int j = 0;
   while(fuente0[i] != '\0')
   {
      concatenacion[i] = fuente0[i];
      i++;
   }
   
   while(fuente1[j] != '\0')
   {
      concatenacion[i + j] = fuente1[j];
      j++;
   }
   concatenacion[i + j] = '\0';
   return;
}
// funcion complement devuelve un puntero a caracter (cadena) y recibe como parametro un puntero a caracter (cadena).
char* complement(char* cadena)
{
    // Cadena resultante toma memoria del mismo tamaño que el parametro.
    char* result = malloc(sizeof(cadena));
    // Recorremos toda la longitud del paramtro cadna
    for(int i = 0,longitud = strlen(cadena); i < longitud; i++)
    {
        // Si el caracter iesimo del paramtro es 1. 
        if(cadena[i] == '1')
        {
            // El caracter iesimo de nuestra cadena resultante sera 0.
            result[i] = '0';
        }
        // Si no si: el caracter iesimo del parametro es 0.
        else if(cadena[i] == '0')
        {
            // El caracter iesimo de nuestra cadena resultante sera 1.
            result[i] = '1';
        }
    }
    // Retornamos puntero a caracter result.
    return result;
}
