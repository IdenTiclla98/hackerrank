/*
                                                      Intro To Tutorial Challenges
   DOCUMENTACION: 
   
   Desafío de ejemplo Este es un simple desafío para empezar. Dado un array ordenado (arr) y un número (v),
   ¿puede imprimir la ubicación de v,índice en la matriz? 
   La siguiente sección describe el formato de entrada. A menudo se puede omitir, si se utilizan métodos incluidos.

   FORMATO DE ENTRADA:
   
   * V: El valor que tiene que ser buscado.
   * N: El tamaño de la matriz.
   * ARR N Números que componen la matriz.
   
   FORMATO DE SALIDA:
   * Salida del índice de V en la matriz.
   
*/
// Declaracion de librerias.
#include <stdio.h>
#include <stdlib.h>
// Declaracion de funcion principal.
int main ()
{
   // Declaracion de v: valor a ser buscado
   int v;
   // Ingresando dato v.
   scanf("%d",&v);
   // Declaracion de n: tamaño del arreglo.
   int n;
   // Ingresando dato n.
   scanf("%d",&n);
   // Declaracion  de puntero a entero arr con memoria dinamica para n enteros.
   int *arr = malloc(sizeof(int) * n);
   // Iterar n veces desde i = 0 hasta n - 1.
   for(int i = 0; i < n; i++)
   {
      // Ingresando dato iesimo en el arreglo.
      scanf("%d",(arr+(i)));
   }
   // Iterando n veces desde i = 0 hasta i = n - 1.
   for(int i = 0;i < n; i++)
   {
      // Si el iesimo elemento del arreglo == valor que buscamos
      if(*(arr+(i)) == v)
      {
         // Imprimimos el indice.
         printf("%i\n",i);
         // Terminar ejecucion con exito.
         return 0;
      }
   }
   
   // Liberando memoria dinamica.
   free(arr);
   // Terminar ejecucion con exito.
   return 0;
}