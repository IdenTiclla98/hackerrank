/*
                     
DOCUMENTACION:
Considere una matriz de cadenas numéricas, sin clasificar, donde cada cadena es un número positivo con cualquier lugar de 1 a 10^6 dígitos.
Clasifique los elementos de la matriz en orden no decreciente (es decir, ascendente)
de sus valores enteros del mundo real e imprima cada elemento de la matriz ordenada en una nueva línea.

FORMATO DE ENTRADA:

* La primera línea contiene un número entero, n, que indica el número de cadenas en no ordenadas.

* Cada una de las n líneas subsiguientes contiene una cadena de enteros que describen un elemento de la matriz.
 Cada cadena está garantizada para representar un entero positivo sin ceros a la izquierda.

*/
// Declaracion de librerias.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// Declaracion de constante MILLON
#define MILLON 1000001
// Prototipos de funciones.
int stringAsInt(const void *pLeft, const void *pRight);
// Declaracion de funcion principal.
int main ()
{
// Declaracion de variable entera n: dimension del arreglo.
int n;
// Ingresando dimension.
scanf("%d",&n);
// Variable buffer que sera utilizada para leer desde 0  a MILLON digitos.
char buffer[MILLON];
// Creando arreglo de strings.
char* arreglo[n];
// Iterando n veces.
for(int i = 0; i < n; i++)
{
   // Leendo primeros 1000000 caracteres si hay menos, leer eso.
   scanf("%1000000s",buffer);
   // Dar memoria necesaria: tamaño del buffer. 
   arreglo[i] = malloc(sizeof(char)*strlen(buffer) + 1);
   // Ingresando en el iesimo elemento.
   strcpy(arreglo[i],buffer);
}

// Aplicando algoritmo de ordenacion (QUICK SORT)

/* Funcion qsort: recibe 4 parametros:
(primerElemento,NumeroDeElementos,El tamaño de cada elemento,funcion:que devuelve un entero)*/
qsort(arreglo, (size_t)n, sizeof(arreglo[0]), stringAsInt);


//Mostrando Arreglo Ordenado Y LIBERANDO.
for(int i = 0; i < n; i++)
{
   printf("%s\n",arreglo[i]);
   free(arreglo[i]);
}
// Terminando ejecucion con exito.
return 0;
}

int stringAsInt(const void *pLeft, const void *pRight)
{
    const char *left = *(const char**)pLeft;
    const char *right = *(const char**)pRight;
    int leftLen = (int)strlen(left);
    int rightLen = (int)strlen(right);
    if (leftLen != rightLen) {
        return leftLen - rightLen;
    } else {
        return strcmp(left, right);
    }
}