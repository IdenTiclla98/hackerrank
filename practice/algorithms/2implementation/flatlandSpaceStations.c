/*
                                          Flatland Space Stations (Estaciones Espaciales Flatland).
DOCUMENTACION:

Flatland es un país con n ciudades, m de las cuales tienen estaciones espaciales.
* Cada ciudad, c_i, está numerada con un índice distinto de 0 a n - 1.
* Cada ciudad c_i está conectada a la ciudad c_i + 1 por un camino bidireccional de 1 km de longitud.
Por ejemplo, si n = 5 y las ciudades c0 y c4 tienen estaciones espaciales, entonces Flatland tiene este aspecto:

c0    c1    c2    c3    c4
0     1     2     3     4
x-----|-----|-----|-----x

Para cada ciudad, determine su dist a la estación espacial más cercana e imprima el máximo de estas dists.

FORMATO DE ENTRADA:

* La primera línea consta de dos enteros separados por el espacio, n y m.
* La segunda línea contiene m enteros separados por el espacio que describen los índices respectivos de cada ciudad
  que tiene una estación espacial. Estos valores son desordenados y únicos.

Se garantiza que habrá por lo menos 1 ciudad con una estación espacial, y ninguna ciudad tiene más de una.

FORMATO DE SALIDA:

Imprima un número entero que indique la dist máxima que un astronauta en una ciudad de Flatland necesitaría
para llegar a la estación espacial más cercana.

*/
// Declaracion de librerias.
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>


// Prototipos de funciones.
int mayorElemento(int distancia[],int n);
// Funcion Principal.
int main(){
    // Declaracion de variable entera n.
    int n; 
    // Declaracion de variabel entera m.
    int m; 
    // Ingresando datos n y m.
    scanf("%d %d",&n,&m);
    // Declarando arreglo de ciudades con dimension n.
    int ciudad[n];
    // Declarando arreglo de estaciones.
    int estacion[m];
    // Inicializando indice de n ciudades.
    for(int i = 0; i < n; i++){
       ciudad[i] = i;
    }
    // Ingresando indice de m estaciones.
    for(int i = 0; i < m; i++){
       scanf("%i",&estacion[i]);
    }
    // Declarando arreglo de dists para cada ciudad.
    int distancia[n];
    // Iterando n veces desde i = 0 hasta i = n - 1.
    for(int i = 0;i < n ; i++){
       // Declaracion de variable entera dMinima que denotara la distancia minima.
       int dMinima;
       // Iterando m veces desde j = 0 hasta j = m - 1.
       for(int j = 0; j < m;j++){
          // Declaracion de variable distancia que sera la diferencia absoluta entre la ciudad y la estacion.
          int dist = abs(ciudad[i] - estacion[j]);
          // Si j es igual a 0.
          if(j == 0){
             // Distancia minima sera la distancia calculada.
             dMinima = dist;
          }
          // Si no Si: la distancia calculada es menor a la distancia encontrada anteriormente.
          else if(dist < dMinima){
             // La distancia minima sera la distancia calculada.
             dMinima = dist;
          }
       }
       // Distancia iesima sera distancia minima.
       distancia[i] = dMinima;
    }
    // Declaracion de variable  max de tipo entero que obtendra el resultado de la funcion mayor elemento.
    int max = mayorElemento(distancia,n);
    // Imprimimos max
    printf("%i\n",max);
    // Terminando ejecucion con exito.
    return 0;
}
// Definicion de funciones.
int mayorElemento(int distancia[],int n){
   // Declaracion de variable max de tipo entero que denotara el mayor elemento encontrado en el arreglo.
   int max;
   // Iterando n veces desde i = 0 hasta i = n - 1.
   for(int i = 0; i < n; i++){
      // Declaracion de variable entera element que sera igual a la distancia iesima.
      int element = distancia[i];
      // Si estamos en la primera iteracion.
      if(i == 0){
         // El elemento mayor sera element.
         max = element;
      }
      // Si no si: element es mayor al anterior mayor encontrado.
      else if(element > max){
         // El elemento mayor sera element.
         max = element;
      }
   }
   // Retornamos max.
   return max;
}