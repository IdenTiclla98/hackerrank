/*
                                                       Grading Students
   DOCUMENTACION:
   
   PROBLEMA:
   
   Universidad HackerLand tiene la siguiente política de clasificación:
   
   * Cada estudiante recibe una calificacion entre 0 y 100.
   * Cualquier calificacion menor a 40 es un grado de reprobación.
   
   Sam es profesor en la universidad y le gusta redondear el grado de cada estudiante de acuerdo con estas reglas:
   
   * Si la diferencia entre el grado y el siguiente múltiplo de 5 es menor de 3, el grado redondo hasta el siguiente múltiplo de 5.
   * Si el valor del grado es menor que 38, no se produce un redondeo, ya que el resultado será aún un grado de falla.
   
   
   Por ejemplo, el grado = 84 será redondeado a 85.
   pero el grado = 29 no será redondeado porque el redondeo daría lugar a un número que es menos de 40.
   
   
   Dado el valor inicial del grado para cada uno de los estudiantes de sam, escriba código para automatizar el proceso de redondeo.
   Para cada grado_i, redondee de acuerdo con las reglas anteriores e imprima el resultado en una nueva línea.
      
   FORMATO DE ENTRADA
   
   * La primera linea contiene un entero que denota la cantidad de estudiantes.
   * Cada linea contiene la calificacion de el estudiante_i (iesimo).
   
*/
// Declaracion de librerias.
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
// Prototipos de funciones.
bool rangoN(int n);
bool rangoCalif(int calif);
// Funcion Principal.
int main ()
{
   // Declaracion de la variable entera n.
   int n;
   // Bucle do - While (Hacer-Mientras).
   do
   {
     // Ingresando Dato n.
     scanf("%d",&n); 
   }
   while(!rangoN(n)); // Mientras no este en el rango.
   
   int *calificaciones = malloc(sizeof(int)*n);
   
   // Iteracion de la variable n desde (i = 0) hasta (n - 1).
   for(int i = 0; i < n;i++)
   {
      // Bucle Hacer- Mientras.
      do
      {
         // Ingresando calificicion iesima.
         scanf("%d",(calificaciones+(i)));
      }
      while(!rangoCalif(*(calificaciones+(i)))); // si la calificacion iesima no esta en el rango.
   }
   
   // Iteracion de la variable n desde (i = 0) hasta (n - 1).
   for(int i = 0; i < n;i++)
   {
      // Diferencia es cuanto le falta a  la calificacion para llegar al proximo multiplo de 5.
      int diferencia = (5 - (*(calificaciones+(i)) % 5)) % 5;
      // La nueva calificacion es el proximo multiplo de 5 desde la calificacion.
      int nuevaCalif = *(calificaciones+(i)) + diferencia;
      
      // si (la nueva calificacion es >= a 40) y  (la diferencia es < a 3).
      if (nuevaCalif >= 40 && diferencia < 3)
      {
         // La calificacion iesima = es la nueva calificacion.
         *(calificaciones+(i)) = nuevaCalif; 
      }
   }
   // Iteracion de la variable n desde (i = 0) Hasta (i = n - 1).
   for(int i = 0; i < n; i++)
   {
      // Imprimimos la calificacion iesima.
      printf("%i\n",*(calificaciones+(i)));
   }
   // Liberar memoria dinamica.
   free(calificaciones);
   // Retornar 0 (indica que no hubo fallo alguno).
   return 0;
}

// Definicion de funciones.

// Funcion rangoN recibe como parametro un entero (n) y devuelve un valor booleano.
bool rangoN(int n)
{
   // Si  (n es mayor o igual a 1) y (n es menor o igual a 60).
   if(n >= 1 && n <= 60)
   {
      // Retornar Verdadero.
      return true;
   }
   // Retornar Falso.
   return false;
}
// Funcion rangoCaliif recibe como parametro un entero (calif) y devuelve un valor booleano.
bool rangoCalif(int calif)
{
   // Si (la calificacion es mayor o igual a cero) y (la calificacion es menor o igual a cien).
   if(calif >= 0 && calif <= 100)
   {
      // Retornar Verdadero.
      return true;
   }
   // Retornar Falso.
   return false;
}