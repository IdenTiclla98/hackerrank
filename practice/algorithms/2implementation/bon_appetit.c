/*Declaracion de librerias*/
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>

/*Declaracion de funciones*/
bool rango_n(int n);
bool rango_k(int k,int n);
bool rango_costos(int costo);
int sumatoria_de_costos(int costos[],int longitud);
int sumatoria_excluyente(int costos[],int longitud,int excluido);
bool rango_charged(int charged,int costos[], int longitud);

/*Declaracion de estructura principal*/
int main(void)
{

    /* Enter your code here. Read input from STDIN. Print output to STDOUT */ 
    
    int n,k;
    int b_charged;

    do
    {
       scanf("%d %d",&n,&k);
    }while(!((rango_n(n))&&(rango_k(k,n))));
    
    int costos[n];

    /*Rellenando arreglo de costo*/
    for(int i=0;i<n;i++)
    {
        do
        {
        scanf("%d",&costos[i]);
        }while(!(rango_costos(costos[i])));
    }
    

    do
    {
        scanf("%d",&b_charged);    
    }while(!(rango_charged(b_charged,costos,n)));
        
   
    int b_actual = sumatoria_excluyente(costos,n,k)/2;
    /*Implementacion de algoritmo*/
    
    if(b_charged-b_actual>0)
    {
        printf("%i\n",b_charged-b_actual);
    }
    else
    {
        printf("Bon Appetit\n");
    }
    
    
    return 0;
}

/*Implementacion de funciones*/
bool rango_n(int n)
{
    if((n>=2)&&(n<=trunc(pow(10,5))))
    {
        return true;
    }
    return false;
}
bool rango_k(int k, int n)
{
    if((k>=0)&&(k<=n))
    {
        return true;
    }
    return false;
}
bool rango_costos(int costo)
{
    if((costo>=0)&&(costo<=trunc(pow(10,4))))
    {
        return true;
    }
    return false;
}
int sumatoria_de_costos(int costos[],int longitud)
{
    int sumatoria = 0;
    for(int i=0;i<longitud;i++)
    {
        sumatoria = sumatoria + costos[i];
    }
    return sumatoria;
}
bool rango_charged(int charged,int costos[], int longitud)
{
    if((charged>=0)&&(charged<=sumatoria_de_costos(costos,longitud)))
    {
        return true;
    }
    return false;
}
int sumatoria_excluyente(int costos[],int longitud,int excluido)
{
    int suma_excluida =  sumatoria_de_costos(costos,longitud)-costos[excluido];
    return suma_excluida;
}