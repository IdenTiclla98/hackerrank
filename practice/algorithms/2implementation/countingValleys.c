/*
                                                   Counting Valleys
   DOCUMENTACION:
   
   Gary es un ávido excursionista.
   Sigue sus caminatas meticulosamente, prestando mucha atención a pequeños detalles como la topografía.
   Durante su última caminata, tomó exactamente N pasos.
   Para cada paso que tomaba, él notó si se trataba de una cuesta arriba o un paso de bajada.
   Las caminatas de Gary comienzan y terminan a nivel del mar. Definimos los siguientes términos:
   
   * Una montaña es una secuencia no vacía de pasos consecutivos sobre el nivel del mar
     comenzando con un paso hacia arriba desde el nivel del mar y terminando con un paso hacia el nivel del mar.

   * Un valle es una secuencia no vacía de pasos consecutivos por debajo del nivel del mar
     comenzando con un paso hacia abajo desde el nivel del mar y terminando con un paso hacia el nivel del mar.
     
   Dada la secuencia de Gary de subir y bajar pasos durante su última caminata, encontrar e imprimir el número de valles que caminó a través.
   
   FORMATO DE ENTRADA

   * La primera línea contiene un número entero, N, que indica el número de pasos en la caminata de Gary.
   * La segunda línea contiene una sola cadena de N caracteres.
    Cada carácter es {U, D} (donde U indica un paso hacia arriba y D indica un paso hacia abajo)
    y el i-ésimo carácter en la cadena describe el paso de Gary durante la caminata.
    
    FORMATO DE SALIDA

   * Imprime un solo entero que denota el número de valles que Gary atravesó durante su caminata.
   
    DATOS EXTRAS:
      Valle: es una llanura entre montañas o alturas, una depresión de la superficie terrestre entre dos vertientes
             con forma inclinada y alargada, que conforma una cuenca hidrográfica en cuyo fondo se aloja un curso fluvial.
             
      Montaña: es una eminencia topográfica superior a 700 m respecto a su base.
               Las montañas se agrupan, a excepción de los volcanes, en cordilleras o sierras.
   
*/ 
// Declaracion de librerias.
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
// Definicion de constante NIVELMAR que sera 0.
#define NIVELMAR 0
// Funcion Pricipal.
int main()
{
    // Declaracion de variable valles de tipo boleano que la utilizaremos como bandera.
    bool valles = false;
    // Declaracion de contador de tipo entero que denotara cuantos valles cruzo Gary.
    int contador = 0;
    // Declaracion de altura del viaje de tipo entero.
    int altura = 0;
    // Declarando dato (n: numero de pasos en la caminata de Gary).
    int n;
    // Ingresando dato n.
    scanf("%d\n",&n);
    // Declaracion de arreglo de caracteres. Con el tamaño de n caracteres.
    char *s = malloc(sizeof(char) * n);
    // Ingresando s
    scanf("%s",s);
    // Iteramos n veces desde i = 0 hasta i = n - 1.
    for(int i = 0; i < n; i++)
    {
       // Si el caracter iesimo es U.
       if(*(s+(i)) == 'U')
       {
          // Aumentamos la altura.
          altura++;
       }
       // Si no si: El caracter iesimo es D.
       else if (*(s+(i)) == 'D')
       {
          // Disminuimos la altura.
          altura--;
       }
       // Si la altura esta bajo el nivel del mar
       if(altura < NIVELMAR)
       {
          // Entonces hay un valle
          valles = true;
       }
       // Si no si: la altura vuelve al nivel del mar y habia un valle
       else if(altura == NIVELMAR && valles == true)
       {
          // Ya no hay valle.
          valles = false;
          // Y el valle que recorrimos lo contamos.
          contador++;
       }
    }
    // Imprimimos contador de valles.
    printf("%i\n",contador);
    // Liberamos la cadena s que dismonia de memoria dinamica.
    free(s);
    // Terminamos ejecucion sin problema alguno.
    return 0;
}
