/*
                                                         Birthday Chocolate        
   DOCUMENTACION:
   
   Lily tiene una barra de chocolate que consiste en una fila de n cuadrados donde cada cuadrado tiene un entero escrito en él.
   Ella quiere compartirlo con Ron para su cumpleaños, que cae en el mes "M" y el día "D".
   Lily sólo quiere dar a Ron un pedazo de chocolate si contiene "M" cuadrados consecutivos cuyos enteros suman a "D".
   
   
   
   Dada "M", "D" y la secuencia de números enteros escrito en cada cuadrado de la barra de chocolate de Lily
   ¿De cuántas maneras puede Lily romper un pedazo de chocolate para darle a Ron diferente?
   
   Por ejemplo, si m = 2, d = 3 y la barra de chocolate contiene n filas de cuadrados con los números enteros [1,2,1,3,2] 
   escritas en ellos de izquierda a derecha, el siguiente diagrama muestra dos formas de romper off una pieza:
   
   FORMATO DE ENTRADA
   
   * La primera línea contiene un número entero que denota n (el número de cuadrados en la barra de chocolate)
   
   * La segunda línea contiene n enteros separados por un espacio Describiendo las respectivas ganancias de S_0, s_1, s_2 ... s (n-1)
   (Los números escritos en consecutiva Cada cuadrado de chocolate)
   
   * La tercera línea contiene dos números enteros separados por espacios
     Describiendo los respectivos valores de d (cumpleaños de Ron) y M (mes de nacimiento de Ron)
   
*/
// Declaracion de librerias.
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// Prototipo de funciones.
bool rangoN(int n);
bool rangoNumeros(int numero);
bool rangoD(int d);
bool rangoM(int m);
// Funcion Principal.
int main ()
{
   // Declaracion de la variable n
   int n;
   // Bucle do- while  (hacer mientras)
   do
   {
      // Ingresando dato n
      scanf("%d",&n);
   }
   while(!rangoN(n)); // Mientras no este en el rango
   
   // Momento en el que se le pide memoria dinamica al ordenador ojo al final la liberamos con free() para que no exista goteo de memoria.
   int *numeros = malloc(sizeof(int)*n);
   
   // Iteracion de variable n desde i=0 hasta i = n-1
   for(int i = 0;i < n; i++)
   {
      // Bucle do while (hacer mientras)
      do
      {
         // Ingresando dato numeros[i] ojo sintaxis sugar
         scanf("%d",(numeros+(i)));
      }
      while(!rangoNumeros(*(numeros+(i)))); // hacer mientras no este en el rango
   }
   // Declaracion de variables d,m
   int d;
   int m;
   // Inicializacion variable entera contador en 0
   int contador = 0;
   do
   {
      scanf("%d %d",&d,&m);
   }
   while(!rangoD(d) || !rangoM(m)); // Repetir si no esta en el rango
   
   // Iteracion de la variable n desde i=0 hasta i=n-1
   for(int i = 0;i < n;i++)
   {
      // Inicializacion variable suma = 0
      int suma = 0;
      // Variable auxiliar para sacar el indice
      int indice = i;
      // Iteramos m desde i=0 hasta i = m-1
      for(int j=0;j < m;j++)
      {
         // sumamos los elementos
         suma = suma + *(numeros+(indice));
         // Incrementamos indice
         indice++;
      }
      // Si la suma da el dia del cumpleaños de Ron
      if(suma == d)
      {
         // Incrementamos el contador
         contador++;
      }
   }
   //Finalmente Imprimimos el contador
   printf("%i\n",contador);
   free(numeros);
   return 0;
}
// Definicion de funciones.
bool rangoN(int n)
{
   if(n >= 1 && n <=100)
   {
      return true;
   }
   return false;
}
bool rangoNumeros(int numero)
{
   if(numero >= 1 && numero <=5)
   {
      return true;
   }
   return false;
}
bool rangoD(int d)
{
   if(d >= 1 && d<=31)
   {
      return true;
   }
   return false;
}
bool rangoM(int m)
{
   if(m >= 1 && m <= 12)
   {
      return true;
   }
   return false;
}