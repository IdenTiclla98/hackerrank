/*
                                          Minimum Distances (Minimum Distances)
DOCUMENTACION:

Considere una matriz de n enteros, A = [a0, a1, ....., an-1]. 
La distancia entre dos índices, i y j, se denomina d_i-j = abs [i - j].

Dada A, encuentre el d_i-j mínimo tal que ai = aj e i! = J.
En otras palabras, encuentre la distancia mínima entre cualquier par de elementos iguales en la matriz. 
Si no existe ese valor, imprima -1.

Nota: |a| Denota el valor absoluto de a.

FORMATO DE ENTRADA:
* La primera línea contiene un entero, n, que indica el tamaño de la matriz A.
* La segunda línea contiene n enteros separados por espacio que describen los elementos respectivos en el array A.

FORMATO DE SALIDA:
* Imprime un solo entero que indica el dij mínimo en A; Si no existe ese valor, imprima -1.

*/
// Declaracion de librerias.
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
// Funcion Principal.
int main(){
// Declaracion de variable entera   
int n; 
// Ingresando dato n: que denota
scanf("%d",&n);
// Declaracion de puntero a enteros con memoria dinamica para n enteros.
int *arr = malloc(sizeof(int) * n);
// Iterando n veces.
for(int i = 0; i < n; i++){
//Ingresando valor iesimo en el arreglo.   
scanf("%d",&arr[i]);
}
// Declaracion de variable entera minDiff inicializada en -1;
bool flag = false;
// Declaracion de minima diferencia.
int minDiff;
// Iteramos n veces.
for(int i = 0; i < n; i++){
   // Iteramos n veces.
   for(int j = 0;j < n;j++){
      // Si el el iesimo elemento del arreglo  es igual j-esimo elemento del arreglo y los indices son diferentes.
      if(arr[i] == arr[j] && i != j){
         // Distancia es igual a al absoluto de la diferencia de los idices. |i - j|
         int distancia = abs(i - j);
         // Si la bandera es falsa.
         if(flag == false)
         {
            // La minima diferencia sera la distancia.
            minDiff = distancia; 
            // Cambiamos la bandera a verdad.
            flag = true;   
         }
         // Por falso.
         else if(distancia < minDiff)
         {
            // Minima diferencia sera igual a la distancia.
            minDiff = distancia;
         }
      }
   }
}
// Si la bandera es falsa.
if(flag == false)
{
   // Imprimimos -1.
   printf("-1");
   // Terminamos ejecucion.
   return 0;
}
// Por falso.
else
{
   // Imprimimos la minima diferencia.
   printf("%d\n",minDiff);   
}
// Liberando memoria dinamica.
free(arr);
// Terminar ejecucion con exito.
return 0;
}
