/*Declarando librerias*/
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>

#define ANCHURA_CHAR 1

/*Prototipos de funciones*/
bool rango_altura(int h);
/*Estructura principal*/
int main()
{
        

    int h[26];
    for(int i = 0; i < 26; i++)
    {
        do
        {
            
           scanf("%d",&h[i]);
           
        }while(!(rango_altura(h[i])));
    }
    
    char* word = (char *)malloc(512000 * sizeof(char));
    scanf("%s",word);
    
    int longitud = strlen(word);
    int mayor = h[0];
    for(int i=0;i<longitud;i++)
    {
        int indice = (int)(word[i]) % 97;
        if(h[indice]>mayor)
        {
            mayor = h[indice];
        }
    }
    

    int resultado = longitud *ANCHURA_CHAR * mayor;
    
    printf("%d\n",resultado);
    return 0;

}
/*Implementacion de funciones*/
bool rango_altura(int h)
{
    if((h>=1)&&(h<=7))
    {
        return true;
    }
    return false;
}