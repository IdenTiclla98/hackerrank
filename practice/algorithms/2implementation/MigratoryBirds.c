/*
                                                         Migratory Birds(Pájaros migratorios).
   DOCUMENTACION
   
   Una bandada de n pájaros está volando a través del continente.
   Cada ave tiene un tipo, y los diferentes tipos son designados por los números de identificación 1,2,3,4 y 5
   
   Dada una matriz de n números enteros donde cada número entero describe el tipo de un pájaro en el rebaño
   encuentre e imprima el número de tipo del pájaro más común.
   Si dos o más tipos de aves son igualmente comunes, elija el tipo con el número de identificación más pequeño
   
   
   FORMATO DE ENTRADA

   La primera línea contiene un número entero que denota n (el número de aves).
   La segunda línea contiene n enteros separados por espacios Describiendo los respectivos números especie de cada pájaro en el rebaño.
   
   FORMATO DE SALIDA

   Imprimir el número de la especie de ave más común;
   si dos o más tipos de aves son comunes Igualmente, elegir el tipo con el número de identificación más pequeño.
      
*/
// Declaracion de librerias
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
// Definicion de constante.
#define LIMIT_ID 5
// Funcion Princiapl
int main(){
    // Declaracion de variable n que sera la cantidad de pajaros.
    int n; 
    // Ingresando dato n.
    scanf("%d",&n);
    // Asignando memoria dinamica a types del tamaño de un entero * n.
    int *types = malloc(sizeof(int) * n);
    // Iterando de i = 0 hasta n - 1
    for(int types_i = 0; types_i < n; types_i++){
       // Ingresando el ID del pajaro iesimo
       scanf("%d",&types[types_i]);
    }
    // your code goes here
    // Declaracion de arreglo para 5 valores los (ID)
    int id[LIMIT_ID];
    // Limpiando valores basura dentro de arreglo id
    for(int i = 0; i < LIMIT_ID; i++)
    {
       id[i] = 0;
    }
    // Iterando la variable n desde i = 0 hasta i = n - 1
    for(int types_i = 0; types_i < n; types_i++)
    {
       // incrementar el arreglo[indice - 1]     ojo -1 es por que nuestro arreglo comienza en 0
       id[types[types_i] - 1] = id[types[types_i] - 1] + 1; 
    }
    int mayor;
    // Sacando el id mayor del arreglo de id
    for(int i = 0; i < LIMIT_ID; i++)
    {
       // Preguntando si es la primera iteracion
       if(i == 0)
       {
          // mayor es igual al elemento iesimo
          mayor = id[i];
       }
       // Por falso
       else
       {
          // Si el elemento iesimo del arreglo de id es mayor a la variable mayor
          if(id[i] > mayor)
          {
             // Mayor sera ese elemento
             mayor = id[i];
          }
       }
    }
    // Iteramos n desde i = 0 hasta n -1
    for(int i = 0; i < n; i++)
    {
       // Si el elemento iesimo del arreglo de id es igual a mayor
       if(id[i] == mayor)
       {
          // Imprimimos la posicion + 1 esto por que el primer elemento empieza en 0
          printf("%i\n",i + 1);
          // Terminamos la ejecucion
          return 0;
       }
    }
    // Terminamos ejecucion
    return 0;
}
