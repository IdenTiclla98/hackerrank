/*
                                          Append and Delete (Agregar y eliminar)
DOCUMENTACION:

Tiene una cadena, s, de letras alfabéticas en inglés en minúsculas. Puede realizar dos tipos de operaciones en s:

1 Aplicar una letra alfabética en minúsculas en inglés al final de la cadena.
2 Seleccione el último carácter de la cadena. Realizar esta operación en una cadena vacía resulta en una cadena vacía.

Dado un entero, k, y dos cadenas, s y t, determinan si puede o no convertir s a t realizando exactamente k
de las operaciones anteriores en s. Si es posible, imprima Sí; De lo contrario, imprima No.

FORMATO DE ENTRADA:

La primera línea contiene una cadena, s, que denotan la cadena inicial.
La segunda línea contiene una cadena, t, que indica la cadena final deseada.
La tercera línea contiene un entero, k, que indica el número de operaciones deseado.

S y t consisten en letras alfabéticas inglesas en minúsculas.

FORMATO DE SALIDA:

Imprimir Sí si puede obtener la cadena t ejecutando exactamente k operaciones en s; De lo contrario, imprima No.

*/
// Declaracion de librerias.
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
// Funcion Pricipal.
int main(){
    // Declaracion de variable puntero a caracter s con memoria dinamica para 512000 caracteres.
    char* s = (char *)malloc(512000 * sizeof(char));
    // Ingresando dato s.
    scanf("%s",s);
    // Declaracion de variable puntero a caracter t con memoria dinamica para 512000 caracteres.
    char* t = (char *)malloc(512000 * sizeof(char));
    // Ingresando dato t.
    scanf("%s",t);
    int k; 
    scanf("%d",&k);
    return 0;
}
