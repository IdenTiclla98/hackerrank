/*
                                          Repeated String(Cadena repetida)
DOCUMENTACION:

Dado un entero, n, encuentre e imprima el número de letra a en las primeras n letras de la cadena infinita de Lilah.

FORMATO DE ENTRADA:

The first line contains a single string, S. 
The second line contains an integer, N.

FORMATO DE SALIDA:

Imprime un  entero que denota el número de letra a en las primeras N letras de la cadena infinita 
creada repitiendo S infinitamente muchas veces.

*/

// Declaracion de funciones.
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>

#define MAXLEN 101
// Funcion Principal.
int main(){
// Declaracion de puntero a caracter con memoria dinamica para MAXLEN caracteres.   
char *s = malloc(sizeof(char)*MAXLEN);   
// Ingresando dato s.
scanf("%s",s);
// Declaracion de longitud de cadena obtiene la longitud de la cadena.
int longitudCadena = strlen(s);
// Declaracion de variable n de tipo n.
long n;
//Ingresando dato n.
scanf("%ld",&n);
// Declaracion de cant_aPal que seran las palabras que existen en la cadena
long cant_aPal = 0;
for(int i = 0; i < longitudCadena; i++){
   if(s[i] == 'a'){
   cant_aPal ++;
   }
}
// La cantidad de palabras que hay en n.
long palabras = n / longitudCadena;
// Cantidad total de a sera: las palabras que entran * la cantidad de a por palabra.
long cant_aTotal = palabras * cant_aPal;
// Las caracteres restantes en los que buscaremos.
long residuo = n % longitudCadena;
//Iterando desde i = 0 hasta (residuo - 1).
for(long i = 0; i < residuo; i++)
{
   // Si el iesimo caracter de la cadena es a.
   if(s[i] == 'a')
   {
      // Aumentamos la cantidad de a totales.
      cant_aTotal++;
   }
}
// Imprimimos la cantidad total de a.
printf("%ld\n",cant_aTotal);
// Terminar ejecucion con exito.
return 0;
}