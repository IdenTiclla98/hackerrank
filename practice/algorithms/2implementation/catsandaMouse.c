/*
                                                   Cats and a Mouse (Gatos y un ratón)
   DOCUMENTACION:

   Dos gatos llamados A y B están de pie en puntos integrales en el eje x.
   El gato A está de pie en el punto X y el gato B está de pie en el punto Y.
   Ambos gatos corren a la misma velocidad, y quieren coger un ratón llamado C que se esconde en el punto integral Z en el eje x.
   ¿Puedes determinar quién atrapará el ratón?
   
   
   Se le dan preguntas (Q) en forma de (X), (Y) y (Z). Para cada consulta, imprima la respuesta apropiada en una nueva línea:
   
   
   * Si el gato (A) coge el ratón primero, imprima Cat A.
   * Si el gato (B) coge el ratón primero, imprima el gato B.
   * Si ambos gatos llegan al ratón al mismo tiempo, imprima el ratón C mientras los dos gatos pelean y escapa el ratón.
   
   FORMATO DE ENTRADA

   La primera línea contiene un único entero, (Q), que indica el número de consultas.
   Cada una de las Q líneas subsiguientes contiene tres enteros separados por el espacio que describen los respectivos valores de:
   X (ubicación del gato A), Y (ubicación del gato B) y (Z) (ubicación del ratón C).
   
   FORMATO DE SALIDA

   En una nueva línea para cada consulta imprima:
      * Cat A si el gato A captura el ratón primero.
      * Cat B si el gato B captura el ratón primero.
      * Mouse C si el ratón escapa.
      
   
*/
// Declaracion de librerias.
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
// Funcion Principal.
int main()
{
    // Declaracion de variable tipo entera (q: numero de consultas a realizar).
    int q; 
    // Ingresando numero de consultas
    scanf("%d",&q);
    // Iterando q veces
    for(int i = 0; i < q; i++)
    {
        // Declaracion de variable entera (x: ubicacion del gato A).
        int x; 
        // Declaracion de variable entera (y: ubicacion del gato B).
        int y; 
        // Declaracion de variable entera (z: ubicacion del raton C).
        int z; 
        // Ingresando Ubicaciones:
        scanf("%d %d %d",&x,&y,&z);
        // Declaracion distancia de x a z tipo flotante.
        float distx_z;
        // Declaracion distancia de y a z tipo flotante.
        float disty_z;
        /*Formula distancia entre puntos
            d = sqrt((x2 - x1)^2+(y2 - y1)^2)
         PERO SOLO USAREMOS
            d = sqrt((x2 - x1)^2)
         DEBIDO A QUE LAS COMPONENTES Y SON SIEMPRE 0.
        */
        // Calculamos distancia de x hacia el raton.
        distx_z = sqrt(pow((z - x),2));
        // Calculamod distancia de y hacia el raton.
        disty_z = sqrt(pow((z - y),2));
        // Si las distancias son igaules.
        if(distx_z == disty_z)
        {
           // Los gatos se pelean y el raton escapa.
           printf("Mouse C\n");
        }
        // si no si: la distancia de y es mayor a la distancia de x
        else if(disty_z > distx_z)
        {
           // El gato A atrapa al raton.
           printf("Cat A\n");
        }
        // Si no si: La distancia de x es mayor a la distancia de y
        else if(distx_z > disty_z)
        {
           // El gato B atrapa al raton
           printf("Cat B\n");
        }
        
    }
    // Finalizar ejecucion  (sin error alguno return 0). 
    return 0;
}