/*Declaracion de librerias*/
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
/*Declaracion de funciones*/
bool rangox1x2(int n);
bool rangov1v2(int n);

/*Estructura principal*/
int main(void)
{
    int x1; 
    int v1; 
    int x2; 
    int v2;
    int contador=0;
    do
    {
        scanf("%d %d %d %d",&x1,&v1,&x2,&v2);
    }while(!rangox1x2(x1)||!(rangox1x2(x2))||!(rangov1v2(v1))||!(rangov1v2(v2)));
    
    
    for(int i=0;i<100000;i++)
    {
        x1=x1+v1;
        x2=x2+v2;
        
        if(x1==x2)
        {
        contador = contador + 1; 
        }
        
    }
    if(contador>0)
    {
        printf("YES\n");
    }
    else
    {
        printf("NO\n");
    }
    
    
    
}



/*Funciones*/
bool rangox1x2(int n)
{
    if((n>=0)&&(n<=10000))
    {
        return true;
    }
    return false;
}
bool rangov1v2(int n)
{
    if((n>=1)&&(n<=10000))
    {
        return true;
    }
    return false;
}