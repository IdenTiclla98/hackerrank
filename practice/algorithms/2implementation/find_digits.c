/*Declaracion de libreriras*/
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
/*Declracion de funciones*/
bool rango_t(int t);
bool rango_n(int n);
int num_digit(int numero);
bool divisibilidad(int a, int b);
/*Estructura principal*/
int main(void){
    int t;
    do
    {
    scanf("%d",&t);
    }while(!(rango_t(t)));
    
    
    for(int i = 0; i < t; i++)
    {
        int n;
        do
        {
          scanf("%d",&n);
          printf("%i\n",num_digit(n));
        }while(!rango_n(n));
        
    }
    return 0;
}
/*Implementacion de las funciones*/
bool rango_t(int t)
{
   if((t>=1)&&(t<=15))
   {
      return true;
   }
   return false;
}
bool rango_n(int n)
{
   if((n>0)&&(n<trunc(pow(10,9))))
   {
      return true;
   }
   return false;
}

int num_digit(int numero)
{
   int copia_numero = numero;
   int longitud = (int)(log10(numero)+1);
   int arreglo[longitud];
   int contador = 0;
   for(int i=0;i<longitud;i++)
   {
      arreglo[i] = copia_numero % 10;
      copia_numero = copia_numero / 10;
      if(divisibilidad(numero,arreglo[i]))
      {
         contador = contador + 1;
      }
   }
   return contador;
}

bool divisibilidad(int a, int b)
{
   if(b==0)
   {
      return false;
   }
   if (a % b==0)
   {
      return true;
   }
   return false;
}