/*
                                          Beautiful Triplets (Tripletas hermosas).
DOCUMENTACION:
Erica escribio una secuencia creciente de n numeros (a0,a1,...,a(n-1)) en su cuaderno.
Ella considera un triple (a_i,a_j,a_k) seria bello si.
* i < j < k.
* a[j] - a[ai] = a[k] - a[j] = d.

Dada la secuencia y el valor de d.
Puedes ayudar a Erica a contar el numero de tripletas hermosas en la secuencia?.

FORMATO DE ENTRADA:
* La primera linea contiene 2 enteros separados por el espacio, n (la longitud de la secuencia) y d (la diferencia hermosa).
* La segunda linea contiene n enteros separados por espacios, describiendo la secuencia creciente de Erica (a0,a1,...,a(n - 1)).

RESTRICCIONES:
* 1 <= n <= 10^4.
* 1 <= d <= 20.
* 0 <=a_i <= 2x10^4.
* a_i > a_(i - 1) for 0 < i <= n-1.

FORMATO DE SALIDA.
* Imprima una sola linea que denota el numero de tripletas hermosas en la secuencia.
*/
// Declaracion de librerias
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

// Funcion Principal.
int main(){
    // Declaracion de contador tipo entero que denotara: (el numero de tripletas hermosas encontradas).
    int contador = 0;
    // Declaracion de variable n tipo entero que denota la (longitud de la secuencia).
    int n;
    // Declaracion de variable d tipo entero que denota la (diferencia hermosa).
    int d;
    // Ingresando dato n.
    scanf("%d %d",&n,&d);
    // Declaracion de arreglo de enteros de tamaño n con dimension n.
    int secuencia[n];
    // Iteramos n veces desde i = 0 hasta i = n - 1.
    for(int i = 0; i < n; i++){
       // Ingresando dato secuencia iesima.
       scanf("%d",&secuencia[i]);
    }
    // Iteramos desde i = 0 hasta i < n - 2.
    for(int i = 0; i < n - 2; i++){
       // Iteramos desde j = i + 1 hasta j < n - 1.
       for(int j = i + 1; j < n - 1; j ++){
          // Calculamos la diferencia entre secuencia j-esima y secuencia iesima.
          int dif1 = secuencia[j] - secuencia[i];
          // Si esa diferencia es mayor a d.
          if(dif1 > d){
            // Cortamos la iteracion.
            break;   
          }
          // Iteramos desde  k = j + 1 hasta k < n.
          for(int k = j + 1; k < n; k++){
            // Calculamos la 2da diferencia entre secuencia k-esima y secuencia j-esima.
            int dif2 = secuencia[k] - secuencia[j]; 
            // Si la diferencia 2 se paso de d.
            if(dif2 > d)
            {
               // Interrumpimos el bucle.
               break;   
            }
            // Al final si no se interrumpieron continuamos con la condicion.
            // Si las diferencias son iguales a d.
            if(dif1==d && dif2==d)
            {
               // Incrementamos el contador en 1.
                contador++;
            }
          }
       }
    }
    // Imprimimos el contador.
    printf("%d\n",contador);
    // Terminando ejecucion con exito.    
    return 0;
}
