"""
                                          Extra Long Factorials (Factoriales extra largos).
DOCUMENTACION:
   Se le da un número entero n. Imprimir el factorial de este número.
ENTRADA:
   La entrada consiste en un numero n , donde: 1 <= n <= 100.
SALIDA:
   Imprima el factorial de este numero.
"""
# Ingresando dato n. 
n = int(input().strip())
# Definicion de funcion factorial.
def factorial(n):
    if n == 1:
        return n
    return n * factorial(n-1)
# Inprimimos el factorial de n.
print(factorial(n))