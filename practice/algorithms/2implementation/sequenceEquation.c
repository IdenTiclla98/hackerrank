/*
                                                         Sequence Equation (Ecuación de Secuencia).                        
DOCUMENTACION:
Se le da una secuencia de n enteros, p (1), p (2), .., p (n). Cada elemento de la secuencia es distinto y satisface 1 <= p (x) <= n.
Para cada x donde 1 <= x <= n, encuentre cualquier entero y Tal que p (p (y)) --- x e imprime el valor de y en una nueva línea.

FORMATO DE ENTRADA:
*  La primera línea contiene un número entero, n, que indica el número de elementos en la secuencia.
*  La segunda línea contiene n enteros separados por el espacio que denotan los respectivos valores de p(1),p(2),..,p(n).

RESTRICCIONES:
* 1 <= n <= 50.
* 1 <= p(x) <= 50 donde: 1 <= x <= n.
* Cada elemento de la secuencia es distinto.

FORMATO DE SALIDA

Para cada x de 1 a n, imprima un entero que denota cualquier Y válido que satisfaga la ecuación p (p (y)) --- x en una nueva línea.
*/
// Declaracion de librerias.
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
// prototipos de funciones.
int busqueda(int values[],int n,int value);
// Funcion Principal.
int main(){
   // Declaracion de variable entera n que denotara la dimension de la secuencia.
   int n;
   // Ingresando dato: n.
   scanf("%d",&n);
   // Declaracion de arreglo de enteros sequence con una dimension n.
   int sequence[n];
   // Iterando n veces desde i = 0 hasta i = n - 1.
   for(int i = 0; i < n; i++){
      // Ingresando dato sequence iesimo.
      scanf("%d",&sequence[i]);
   }
   // Declaracion de variable entera y.
   int y;
   // Iterando n veces, desde i = 0 hasta i = n - 1.
   for(int i = 0; i < n; i++){
       // Y es igual a la busqueda del valor de la posicion de i en el arreglo.
       y = busqueda(sequence,n,(busqueda(sequence,n,i + 1)) + 1) + 1;
       // Imprimiendo y.
       printf("%d\n",y);
   }
   // Terminando ejecucion con exito.
   return 0;
}
// Definicion de funciones.
int busqueda(int values[],int n,int value)
{
   // Iterando la dimension desde i = 0 hasta i = n - 1.
   for(int i = 0; i < n; i++){
      // Si el elemento iesimo de values es igual al valor
      if(values[i] == value){
         // Retornar la posicion.
         return i;
      }
   }
   // Caso contrario a no encontrar el valor retornar 0.
   return 0;
}