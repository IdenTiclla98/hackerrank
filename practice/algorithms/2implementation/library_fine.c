//                                              Library fine

   /*
                                       Premisas para cobrar la multa 
                                    
      -Si el libro se devuelve en o antes de la fecha de devolución esperada, no se cobrará multa multa = 0
      -Si el libro se devuelve después del día de retorno esperado, pero aún dentro del mismo mes y año calendario
      que la fecha de devolución esperada multa = 15 hackos x (el número de días de retraso)
      -Si el libro es devuelto después del mes de retorno esperado pero aún dentro del mismo año calendario que la fecha de devolución esperada,
      la multa = 500 hackos x (el número de meses de retraso)
      -Si el libro es devuelto después del año calendario en el que se esperaba, hay una multa fija de 10000 hackos
   
   */
   
   /*
                                       formato de entrada
      -la primera línea contiene 3 enteros separados por el espacio que indican el día, el mes y el año respectivos
      en los que el libro fue realmente devuelto
      -La segunda línea contiene 3 enteros separados por espacio que indican el día, mes y año respectivos
      en los que se esperaba que el libro se devolviera (fecha de vencimiento)
   
   */


/*Declarando librerias*/
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>

#define MULTA_DIA 15
#define MULTA_MES 500
/*Prototipos de funciones*/
bool rango_dia(int dia);
bool rango_mes(int mes);
bool rango_year(int year);

/*Estructura principal*/
int main()
{
   

   int multa = 0;
   
    
    int d1; 
    int m1; 
    int y1; 
    do
    {
      scanf("%d %d %d",&d1,&m1,&y1);
    }while(!(rango_dia(d1)&&rango_mes(m1)&&rango_year(y1)));
    
    
    int d2; 
    int m2; 
    int y2;
    do
    {
      scanf("%d %d %d",&d2,&m2,&y2);
    }while(!(rango_dia(d2)&&rango_mes(m2)&&rango_year(y2)));
    /*Si en caso no hay multa*/
    if((d1<=d2)&&(m1<=m2)&&(y1<=y2))
    {
       multa = 0;
    }
    /*Multa de los dias*/
    else if((d1>d2)&&(m1==m2)&&(y1==y2))
    {
       multa = (d1-d2) * MULTA_DIA; 
    }
    /*multa de los meses*/
    else if((m1>m2)&&(y1==y2))
    {
       multa = (m1-m2) *MULTA_MES;
    }
    /*Multa anual*/
    else if(y1>y2)
    {
       multa = 10000;
    }
    printf("%i\n",multa);
    
    return 0;
}
/*Implementacion de funciones*/
bool rango_dia(int dia)
{
   if((dia>=1)&&(dia<=31))
   {
      return true;
   }
   return false;
}
bool rango_mes(int mes)
{
   if((mes>=1)&&(mes<=12))
   {
      return true;
   }
   return false;
}
bool rango_year(int year)
{
   if((year>=1)&&(year<=3000))
   {
      return true;
   }
   return false;
}