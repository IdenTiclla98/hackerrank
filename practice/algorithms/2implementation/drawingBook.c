/*
                                                            Drawing Book
    DOCUMENTACION:
     El profesor de dibujo de Brie pide a su clase que abra su libro de n páginas al número p.
     Brie puede empezar a girar las páginas desde:
     el frente del libro (es decir, la página número 1) o desde la parte posterior del libro (es decir, número de página n)
     y siempre gira páginas uno a uno (en lugar de saltar a través de Varias páginas a la vez). Cuando abre el libro, la página 1 está siempre a la derecha:
        
     Cada página en el libro tiene dos lados, delantero y trasero
     a excepción de la última página que puede tener solamente una cara delantera dependiendo del número total de páginas del libro
     Tiene solo cara delantera cuando el numero de paginas es impar caso contrario tiene cara delantera y trasera.
     
     (vea las secciones de explicación abajo para diagramas adicionales).
        
     Dada N y P, busque e imprima el número mínimo de páginas que Brie debe girar para llegar a la página P.
    
    FORMATO DE ENTRADA:

        * La primera línea contiene un número entero, n, que indica el número de páginas del libro.
        * La segunda línea contiene un número entero, p, que indica la página a la que la maestra de Brie quiere que se vaya.
        
        FORMATO DE SALIDA:
    
        * Imprimir un entero que indique el número mínimo de páginas Brie debe girar para llegar a la página P.
    
    
*/
// Declaracion de librerias.
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
// Definicion de funciones.

int solve(int n, int p)
{
    /*Declaracion de variable mitad tipo entera que obtiene:
    el numero de pagina en la mitad del libro.*/
    int mitad = n / 2;
    // Declaracion de variable paginas que seran las paginas que se voltearon para llegar.
    int paginas = 0;
    // Si la pagina buscada es superior a la mitad
    if(p > mitad)
    {
        // Ojear desde atras.
        
        // El numero de paginas a recorrer sera:
        // La longitud del libro - pagina buscada
        int conPag = n - p;
        // Si el numero de conPag > 1
        if (conPag > 1)
        {
            // paginas = conPag recorridas / 2.
            paginas = round(conPag / 2);
        }
        // Si la conPag es 1 y el numero de conPag es divisible entre 2.
        else if(conPag == 1 && n % 2 == 0 )
        {
            // Retornar 1;
            return 1;
        }
    
    }
    // Si no si: el numero de conPag es mayor a 1.
    else if(p > 1)
    {
        // Ojear desde adelante
        // paginas = numero de conPag / 2
        paginas = round(p / 2);
    }
    // Devolvemos las conPag.
    return paginas;
    
}
// Funcion principal.
int main()
{
    // Declaracion de variable entera n que denota numero de paginas.
    int n; 
    // Ingresando dato n.
    scanf("%d", &n);
    // Declaracion de varible entera p que denota la pagina a la que se quiere llegar.
    int p; 
    // Ingresando dato p.
    scanf("%d", &p);
    // Variable result tipo entero almacena la solucion.
    int result = solve(n, p);
    // Imprimiendo el resultado.
    printf("%d\n", result);
    // Termniando la ejecucion con exito.
    return 0;
}
