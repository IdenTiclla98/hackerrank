/*                                                          Save_the_prisoner.c
Una prisión tiene N prisioneros, y cada prisionero tiene un número de identificación único, S, que van de 1 a N.Hay dulces M que deben ser distribuidos a los prisioneros.
El carcelero decide que la manera más justa de hacerlo es sentando a los prisioneros en un círculo (ordenados por S ascendente), y luego, comenzando con un S aleatorio
, distribuya un caramelo a la vez a cada prisionero numerado secuencialmente hasta que todos los dulces M repartido. 
Por ejemplo, si el carcelero selecciona el prisionero S = 2, entonces su orden de distribución sería (2,3,4,5 ..., N-1, N, 1,2,3,4 ...) 
hasta que todos los M Los dulces se distribuyen.

Pero espera, hay una trampa, ¡el último dulce está envenenado!
¿Puede encontrar e imprimir el número de identificación del último preso para recibir un dulce para que pueda ser advertido?
*/


/*Declarando librerias*/
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>  
#include <stdbool.h>
/*Prototipos de funciones*/
bool primer_rango(int num);
bool segundo_rango(int num);
/*Estructura principal*/
int main() {

    /* Enter your code here. Read input from STDIN. Print output to STDOUT */  
    int t;
    int n,m,s;
   //  int j;
    do
    {
       scanf("%d",&t);
       /*The first line contains an integer,t, denoting the number of test cases. */
    }while(!(primer_rango(t)));
    
    for(int i=0;i<t;i++)
    {
       do
       {
          scanf("%d %d %d",&n,&m,&s);
          /* N(el número de presos), M(el número de dulces), y S(el ID de prisionero).*/
       }while(!(segundo_rango(n)&&segundo_rango(m)&&segundo_rango(s)));
       
<<<<<<< HEAD
       /*Explicacion:
       
       
       
       
       
       */
=======
>>>>>>> 8d9f95ee3e55b6142e76ad098ec986725fe21995
       if((m+s-1)%n==0)
       {
          printf("%i\n",n);
       }
       else
       {
          printf("%i\n",(m+s-1)%n);
       }
    }  

    return 0;
}
/*Implementacion de funciones*/
bool primer_rango(int num)
{
   if((num>=1)&&(num<=100))
   {
      return true;
   }
   return false;
}
bool segundo_rango(int num)
{
   if((num>=1)&&(num<=trunc(pow(10,9))))
   {
      return true;
   }
   return false;
}