/*
                                                Cavity Map (Mapa de la cavidad).
DOCUMENTACION:

Se le da un mapa cuadrado de tamaño n x n.
Cada celda del mapa tiene un valor que indica su profundidad.
Llamaremos a una celda del mapa una cavidad si y sólo si esta célula no está en el borde del mapa
y cada célula adyacente a ella tiene una profundidad estrictamente menor.
Dos células son adyacentes si tienen un lado común (borde).

Necesita encontrar todas las cavidades en el mapa y representarlas con el carácter en mayúsculas X.

FORMATO DE ENTRADA:

* La primera línea contiene un entero, N, que indica el tamaño del mapa.
* Cada una de las siguientes N líneas contiene N dígitos positivos sin espacios.
* Cada dígito (1-9) indica la profundidad del área apropiada.

FORMATO DE SALIDA:

* Salida N líneas, denotando el mapa resultante. Cada cavidad debe ser reemplazada por el carácter X

EXPLICACION:

* Las dos celdas con la profundidad de 9 cumplen todas las condiciones de la definición de Cavidad
  y han sido reemplazadas por X.

*/
// Declaracion de librerias.
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
// Definicion de constante CAV
#define CAV 'X'
// Funcion Principal.
int main(){
   // Declaracion de variable entera n.
   int n;
   // Ingresando dato n.
   scanf("%d",&n);
   // Declaracion de matriz de enteros con dimension n x n.
   char matrix[n][n];
   // Ingresando elementos a la matriz.
   for(int i = 0; i < n; i++){
      // Declaracion de variable cadena arreglo de caracteres.
      char* cadena = malloc(sizeof(char)*n);
      // Ingresando cadena.
      scanf("%s",cadena);
      // Iterando columnas.
      for(int j = 0; j < n; j++){
         // Ingresando en la  fila i columna j el elemento j-esimo de cadena.
         matrix[i][j] = cadena[j];   
      }
      free(cadena);
   }
   // Iterando filas.
   for(int i = 0; i < n; i++){
      // Iterando Columnas.
      for(int j = 0; j < n; j++){
         // Si estamos entre la 1ra y la ultima fila && Si estamos entre la 1ra Columna y Ultima columna.
         if((i > 0 && i < n - 1)&& (j > 0 && j < n - 1)){
            int element = matrix[i][j];
            int a = matrix[i][j - 1];
            int b = matrix[i - 1][j];
            int c = matrix[i][j + 1];
            int d = matrix[i + 1][j];
            // CONDICION: Si el elemento es mayor a sus cuadro lados adyancentes.
            if(element > a && element > b && element > c && element > d){
               // Imprimimos el caracter CAV.
               printf("%c",CAV);
            }
            // Si no:
            else{
               // Imprimimos el caracter matrix[i][j].
               printf("%c",matrix[i][j]);
            }
         }
         // Si no:
         else{
            // Imprimimos el caracter matrix[i][j].
            printf("%c",matrix[i][j]);
         }
      }
      // Imprimimos salto de linea.
      printf("\n");
   }
   // Terminamos ejecucion del programa.
   return 0;
}
