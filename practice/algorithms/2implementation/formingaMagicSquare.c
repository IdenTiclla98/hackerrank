/*
                                       Forming a Magic Square (Formando el cuadrado mágico).
DOCUMENTACION:

Definimos un cuadrado mágico como una matriz n x n de enteros positivos desde de 1 a n ^ 2
donde la suma de cualquier fila, columna o diagonal (de longitud n)
es siempre igual al mismo número (es decir, la constante mágica).
Consideremos una matriz 3 x 3, s, de enteros en el rango inclusivo [1,9].
Podemos convertir cualquier dígito, a, a cualquier otro dígito, b, en el rango [1,9] al costo | a-b |.
Dado s, convertirlo en un cuadrado mágico a un costo mínimo cambiando cero o más de sus dígitos.
Luego imprima este costo en una nueva línea.
NOTA:
* El cuadrado mágico resultante debe contener números enteros distintos en el rango inclusivo [1,9].
FORMATO DE ENTRADA:

Hay 3 líneas de entrada:
* Cada línea describe una fila de la matriz en forma de 3 enteros
  separados por el espacio que denotan los respectivos primer, segundo y tercer elementos de esa fila.
RESTRICCIONES:
* Todos los enteros en s están en el rango inclusivo [1,9].
FORMATO DE SALIDA:
* Imprima un entero que indique el coste mínimo de convertir la matriz s en un cuadrado mágico.
*/
// Declarando librerias.
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#define MAGIC 15
// Funcion Principal.
int main(){
    // Declaracion de matriz s con una dimension de 3 x 3.
    int s[3][3];
    
    
    // Iterando las filas (3), desde i = 0 hasta i = 3 - 1.
    for(int i = 0; i < 3; i++){
       // Iterando las Columnas (3), desde j = 0 hasta j = 3 -1.
       for(int j = 0; j < 3; j++){
          // Ingresando elemento en la posicion i,j en la matriz.
          scanf("%d",&s[i][j]);
       }
    }
    for(int i = 0; i < 3; i++){
       int sumFila = 0;
       for(int j = 0; j < 3; j++){
          sumFila += s[i][j];
       }
       printf("%d\n",sumFila);
    }
    printf("\n");
    for(int j = 0; j < 3;j++){
       int sumColumna = 0;
       for(int i = 0; i < 3; i++){
          sumColumna += s[i][j];
       }
       printf("%d\n",sumColumna);
    }
    
    //  Print the minimum cost of converting 's' into a magic square.
    
    // Terminando ejecucion con exito.
    return 0;
}
