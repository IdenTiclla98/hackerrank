/*
                                                Fair Rations (Raciones justas)
DOCUMENTACION:
Usted es el gobernante de RankHacker Castle y hoy esta distribuyendo pan a una fila recta de N personas. 
Cada persona iesima donde (1<=i<=N) ya tiene i Rebanadas de pan.

Los tiempos son dificiles y las reservas de su castillo estan disminuyendo, por lo que debe distribuir como pocos panes
sea posible de acuerdo a las siguientes reglas:

1 Cada vez que le das una rebanada de pan a una persona i, tambien debe darle una rebanada de pan a la persona de atras de 
ellas. 
En otras palabras, Solo se puede dar una rebanada de pan a cada persona adyantes a la vez.

2 Despues de que todo el pan se distribuye, cada persona debe tener un numero par de panes.

Dado el numero de rebanadas que ya tienen cada ciudadano encuentre e imprima el numero minimo de panes que se debe
distribuir para satisfacer las dos reglas anteriores. Si esto no es posible imprima NO.

FORMATO DE ENTRADA:

* La primero linea contiene un numero entero, N, denota el numero de sujetos en la linea de pan.
* La segunda linea contiene N enteros separados por espacio (Describiendo las respectivas rebanadas de pan ya poseidos
  por cada persona en la linea (i.e. B0,B1,....,B(N-1))).
  
FORMATO DE SALIDA:

Imprima un solo numero entero que indique el numero minimo de panes que debe distribuir a las personas adyacentes
en la linea para que cada persona tenga un numero par de panes; si no es posible hacer, esto. Imprima NO.

*/
// DECLARACION DE LIBRERIAS.
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
// PROTOTIPOS DE FUNCIONES.
bool esPar(int num);
bool elementosPares(int persona[],int n);
// FUNCION PRINCIPAL
int main(){
    // Declaracion de variable entera n que denotara: la cantidad de personas en la fila.
    int n; 
    // Ingresando dato n.
    scanf("%d",&n);
    // Declarando arreglo de enteros personas con una dimension n.
    int persona[n];
    // Iterando n veces desde i = 0 hasta i = n - 1.
    for(int i = 0; i < n; i++){
       scanf("%d",&persona[i]);
    }
    // Declaracion de variable contador de tipo entero inicializada en 0.
    int contador = 0;
    // Iterando n veces desde i = 0 hasta i = n - 2.
    for(int i = 0; i < n - 1; i++){
       // CONDICION: Si el elemento iesimo no es par.
       if(!esPar(persona[i])){
          // Incrementamos en 1 a la persona iesima.
          persona[i]++;
          // Incrementamos en 1 a la persona  siguiente a la iesima.
          persona[i + 1]++;
          // Como repartimos 2 panes  le sumamos 2 a el contador.
          contador = contador + 2;
       }
    }
    // CONDICION: Si todos los elementos del arreglo son pares.
    if(elementosPares(persona,n)){
       // Imprimimos la variable contador.
       printf("%d\n",contador);
    }
    // Si no
    else{
       // Imprimimos No.
       printf("NO\n");
    }
    // Terminando ejecucion con exito.
    return 0;
}

// DEFINICION DE FUNCIONES
bool esPar(int num){
   // Si el residuo de dividir num por 2 es igual a cero.
   if(num % 2 == 0){
      // Retornamos verdad.
      return true;
   }
   // Retornamos falso.
   return false;;
}

bool elementosPares(int persona[],int n){
   // Iteramos la dimension desde i = 0 hasta i = n - 1.
   for(int i = 0; i < n; i++){
      // Si el elemento iesmo de persona no es par.
      if(!esPar(persona[i])){
         // Retornamos falso.
         return false;
      }
   }
   // Retornamos verdad.
   return true;
}