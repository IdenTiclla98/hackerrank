/*
Explicacion:

Jumping_on_the_Clouds -----> jump_clouds2.c
                                                       
    
Emma está jugando un nuevo juego móvil con n nubes numeradas de 0 a (n-1).
Un jugador comienza inicialmente en la nube C0, y debe saltar a la nube C (n-1)
En cada paso, puede saltar de cualquier nube i a nube i + 1 o nube i + 2.
Hay dos tipos de nubes, nubes ordinarias y nubes de tormenta.
El juego termina si Emma salta sobre una nube de tormenta, pero si llega a la última nube (es decir, c (n-1)), ¡Ella gana el juego!
¿Puedes encontrar el número mínimo de saltos que Emma debe hacer para ganar el juego? 
Está garantizado que las nubes C0 y C (n-1) son nubes ordinarias y siempre es posible ganar el juego                                               
                                                   
*/


/*Declaracion de librerias*/
#include<stdio.h>
#include<stdbool.h>
#include<math.h>
/*Declaracion de funciones*/
bool rango_n(int n);
bool binario(int bin);
/*Funcion Principal*/
int main (void)
{
   int n;
   do
   {
      
      scanf("%d",&n);
      
   }while(rango_n(n));
   int nubes[n];
   nubes[0] = 0;
   nubes[n-1] = 0;
   for(int i=0;i<n;i++)
   {
      do
      {
         
         scanf("%d",&nubes[i]);
         
      }while(binario(nubes[i]) || !((nubes[0]==0)&&(nubes[n - 1]==0)));
   }
   
   
}
/*Definicion de Funciones*/
bool rango_n(int n)
{
   if((n>=2)&&(n<=100))
   {
      return false;
   }
   return true;
}
bool binario(int bin)
{
   if((bin==1)||(bin==0))
   {
      return false;
   }
   return true;
}