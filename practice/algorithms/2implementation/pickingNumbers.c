/*
                              Picking Numbers (Selección de números)
   DOCUMENTACION:
   
   Dada una matriz de números enteros
   busque e imprima el número máximo de enteros que puede seleccionar de la matriz
   de tal manera que la diferencia absoluta entre dos de los enteros elegidos sea <= 1.
   
   FORMATO DE ENTRADA.

   * La primera línea contiene un solo entero, n, que indica el tamaño de la matriz.
   * La segunda línea contiene n enteros separados por el espacio que describen
     los valores respectivos de a0, a1, ...., a (n-1).
     
   Formato de salida

   * Un entero único que indica el número máximo de números enteros que puede elegir en la matriz
     de tal manera que:
     la diferencia absoluta entre dos cualquiera de los enteros elegidos sea <= 1.

*/
// Declarando librerias.
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
// Funcion Principal.
int main()
{
    // Ingresando dato n: Tamaño de la matriz.
    int n; 
    // Ingresando dato n.
    scanf("%d",&n);
    // Creando vector tipo entero arr con memoria dinamica para n enteros.
    int *arr = malloc(sizeof(int) * n);
    // Iterando n veces desde i = 0 hasta i = n - 1.
    for(int i = 0; i < n; i++)
    {
       // Ingresando dato iesimo en el arreglo.
       scanf("%d",&arr[i]);
    }
    
    
    // Terminar ejecucion con exito.
    return 0;
}
