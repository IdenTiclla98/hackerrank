/*
                                                            The hurdle Race (La carrera de obstáculos)
   DOCUMENTACION:
   
   Dan está jugando un videojuego en el que su personaje compite en una carrera de obstáculos saltando sobre n vallas con alturas h0, h1, .., h (n - 1). 
   Inicialmente puede saltar una altura máxima de k unidades, pero tiene un suministro ilimitado de bebidas mágicas que le ayudan a saltar más alto!
   Cada vez que Dan bebe una bebida mágica, la altura máxima que puede saltar durante la carrera aumenta en 1 unidad.

   Dado n, k, y las alturas de todos los obstáculos, encuentre e imprima el número mínimo de bebidas mágicas que Dan debe beber para completar la carrera.
   
   FORMATO DE ENTRADA

      * La primera línea contiene dos enteros separados por espacio que describen:
      
       los valores respectivos de n (el número de obstáculos) y k (la altura máxima que puede saltar sin consumir ninguna bebida).

      * La segunda línea contiene n enteros separados por el espacio que describen los respectivos valores de h0, h1, ..., h (n-1).
   
   FORMATO DE SALIDA

      * Imprima un número entero que indique el número mínimo de bebidas mágicas que Dan debe beber para completar la carrera de obstáculos.
   
   LINK: https://www.hackerrank.com/challenges/the-hurdle-race

*/
// Declaracion de librerias
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
// Funcion Principal
int main()
{
    // Declaracion de variable entera n (numero de obstaculos).
    int n; 
    // Declaracion de variable entera k (la altura maxima que puede saltar sin consumir ninguna bebida).
    int k; 
    // Ingresando datos n y k respectivamente.
    scanf("%d %d",&n,&k);
    // Declaracion de puntero a entero height  con memoria dinamica para almacenar n enteros.
    int *height = malloc(sizeof(int) * n);
    // Iterando la variable n desde height_i = 0 hasta n - 1. 
    for(int height_i = 0; height_i < n; height_i++)
    {
       // Ingresando dato altura iesima
       scanf("%d",&height[height_i]);
    }
    // your code goes here
    // Declaracion de Varible mayor que almacenara la altura maxima
    int mayor;
    int botellas;
    // Iterara desde i = 0 hasta n - 1
    for(int height_i = 0; height_i < n; height_i++)
    {
       if(height_i == 0)
       {
          mayor = height[height_i];
       }
       else if(height[height_i] > mayor)
       {
          mayor = height[height_i];
       }
    }
    botellas = mayor - k;
    if(botellas > 0)
    {
       printf("%i\n",botellas);
    }
    else
    {
       printf("0\n");
    }
    // Termina ejecucion del programa indicando exito.
    return 0;
}