/*
                                                         Day of the programmer
   DOCUMENTACION:
   
   Marie inventó una (Máquina del Tiempo) y quiere probarla viajando por el tiempo para visitar Rusia en el (Día del Programador)
   (el día 256 del año) durante un año en la gama inclusiva de 1700 a 2700..
   
   De 1700 a 1917, el calendario oficial de Rusia era el calendario juliano; Desde 1919 utilizaron el sistema del calendario gregoriano.
   La transición del sistema de calendario juliano al calendario gregoriano ocurrió en 1918
   cuando al día siguiente después del 31 de enero fue el 14 de febrero. 
   Esto significa que en 1918, el 14 de febrero fue el 32º día del año en Rusia.
   
   En ambos sistemas de calendario:
   febrero es el único mes con una cantidad variable de días;
   
   Febrero:
   Tiene 29 días durante un año bisiesto
   y 28 días durante todos los demás años.
   
   En el calendario juliano, los años bisiestos son divisibles por 4;
   
   En el calendario gregoriano, los años bisiestos son uno de los siguientes:
   
   * Divisible por 400.
   * Divisible por 4 y no divisible por 100
   
   Dado un año, y, encuentre la fecha del 256 to día de ese año según el calendario oficial ruso durante ese año.
   Luego imprima en el formato dd.m.yyyy, donde dd es el día de dos dígitos, mm es el mes de dos dígitos y yyyy es y.
   
   FORMATO DE ENTRADA
   
   - Un entero entero que denota el año y.
   
   FORMATO DE SALIDA

   Imprima la fecha completa del Día del programador durante el año y en el formato (dd.mm.yyyy): 
   donde dd es el día de dos dígitos, mm es el mes de dos dígitos y yyyy es y.
   
   
   EXTRAS:
   La reforma gregoriana modificó el esquema del calendario Juliano de los años bisiestos de la siguiente manera:
   Cada año que es exactamente divisible por cuatro es un año bisiesto, excepto por año que son exactamente divisible por 100
   pero estos años centurial son bisiestos si son exactamente divisibles por 400.
   Por ejemplo, los años 1700, 1800 y 1900 son no los años bisiestos, pero los años 1600 y 2000 son.
*/

// Declaracion de librerias
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#define DAYPROGRAMMER

// Prototipos de funciones
bool julianCalendar(int year);
bool gregorianCalendar(int year);

// Funcion principal
int main() 
{
    // Declaracion de variable  year que sera el año   
    int year; 
    // Ingresando dato año
    scanf("%d", &year);
    // Declaracion de dd que sera el dia
    int dd = 0;
    // Declaracion mm que sera el mes
    int mm = 9;
    
    // Si el año esta en el rango de calendario juliano
    if(julianCalendar(year))
    {
       // Si el año es bisiesto
      if(year % 4 == 0)
      {
         // EL dia sera DIA DEL PROGRAMADOR - 244
         dd = DAYPROGRAMMER - 244;
      }
      else
      {
         // EL dia sera DIA DEL PROGRAMADOR - 243
         dd = DAYPROGRAMMER - 243;
      }
    }
    // Si no si: el año esta en el rango de años gregorianos
    else if(gregorianCalendar(year))
    {
       // Si el año es multiplo de 400 o el año es multiplo de 4 pero no de 100
       if((year % 400 == 0) || ((year % 4 == 0) && !(year % 100 == 0)))
       {
          // EL dia sera DIA DEL PROGRAMADOR - 244
          dd = DAYPROGRAMMER - 244;
       }
       else
       {
          // EL dia sera DIA DEL PROGRAMADOR - 243
          dd = DAYPROGRAMMER - 243;
       }
    }
    else
    {
       // Si no cumple ninguna el dia sera 26
       dd = 26;
    }
    // Imprimiendo las fechas
    printf("%i.0%i.%i\n",dd,mm,year);
   //  printf("%s\n", result);
    return 0;
}

bool julianCalendar(int year)
{
   if(year >= 1700 && year <= 1917)
   {
      return true;
   }
   return false;
}
bool gregorianCalendar(int year)
{
   if(year >= 1919)
   {
      return true;
   }
   return false;
}