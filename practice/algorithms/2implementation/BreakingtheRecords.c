/*
                                                Breaking The Records
   DOCUMENTACION:
   
   
   María juega N juegos de baloncesto de la universidad en una temporada.
   Porque quiere ser un profesional, que rastrea su punto conseguido por Secuencialmente 
   juego en una matriz puntuación = [S0, S1 ... s (n-1)].
   Después de cada juego i, se comprueba para ver si su puntuación S_I rompe récord de mayor cantidad de oro Puntos menos hasta ahora anotados 
   durante esa temporada.

   Dada gama de María de las puntuaciones para una temporada de n juegos:
   encontrar e imprimir el número de veces que se rompe el récord de puntos más y menos marcados durante la temporada.
   
   NOTA:
   Asume su récord de puntos más y menos en el inicio de la temporada son el número de puntos anotados durante el primer partido de la temporada.
   
   
   FORMATO DE ENTRADA:
   
   - La primera línea contiene un número entero que denota n (el número de juegos).
   - La segunda línea contiene n enteros separados por espacios Describiendo las respectivas ganancias de s_o, s_1, ...., s_n-1.
   
   FORMATO DE SALIDA
   - Imprimir dos números enteros separados por un espacio
     Describiendo los respectivos números de veces su mejor (más alta) Mayor puntuación y su peor (más bajo) disminuye la puntuación.
*/

// Declaracion de librerias.
#include <stdio.h>
#include <stdlib.h>
// Declaracion de Funcion Principal.
int main ()
{
   // Declaracion de variable n.
   int n;
   // Ingresando varibale n.
   scanf("%d",&n);
   // Declaracion de arreglo dinamico scores que almacenara n enteros
   int *scores = malloc(sizeof(int) * n);
   // iterando n veces desde score_i = 0 hasta n - 1
   for(int score_i = 0; score_i < n; score_i++)
   {
      // Ingresando al arreglo el valor score_i (iesimo)
      scanf("%d",(scores+(score_i)));
   }
   // Declaracion Variables contadores
   // Cuantas veces se rompio el record
   int highestScore = 0;
   // Cuantos veces bajo de su record
   int worstScore = 0;
   
   // Declaracion de element que sera un elemento del arreglo de scores
   int element;
   // Mejor Record
   int bestRecord;
   // Pesimo Record
   int worstRecord; 
   // Iterando la variable n desde i = 0 hasta n - 1
   for(int i = 0; i < n;i++)
   {
      // En la primera iteracion la variable firstGame obtiene su valor.
      if(i == 0)
      {
         // Obtenemos el primer puntaje para el mejor record y el peor record
         bestRecord = *(scores+(i));
         worstRecord = *(scores+(i));
      }
      // Si no estamos en la primera iteracion
      else
      {
         // Elemento es scores en la posicion iesima
         element = *(scores+(i));
         // Si el elemento es > al mejor record
         if(element > bestRecord)
         {
            // EL nuevo mejor record sera ese elemento
            bestRecord = element;
            // Incrementamos el contador de las veces que se rompio el mejor record.
            highestScore++;
         }
         // Por falso si ese elemento es menor al peor Record
         else if (element < worstRecord)
         {
            // El nuevo peor record sera ese elemento.
            worstRecord = element;
            // Incrementamos el contador de las veces que se rompio el peor record.
            worstScore++;
         }
      }
   }
   // Imprimimos los contadores las veces que se rompio el mejor record y el peor record.
   printf("%i %i\n",highestScore,worstScore);
   // Devolvemos la memoria al sistema
   free(scores);
   // Retornamos 0 indicando que no hubo problema alguno.
   return 0;
}
