/*Declaracion de librerias*/
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
/*Prototipos de funciones*/
bool rango_n(int n);
bool rango_k(int k ,int n);
bool rango_binario(int bin);

/*Estructura principal*/
int main()
{
    int n; 
    int k; 
    int puntos = 100;
    int res;
    do
    {
      scanf("%d %d",&n,&k);
    }while(!(rango_n(n)&&rango_k(k,n))&&(n % k == 0));
    
    int arreglo[n];
    /*int *c = malloc(sizeof(int) * n);
    for(int c_i = 0; c_i < n; c_i++){
       scanf("%d",&c[c_i]);
    }
    */
    for(int i=0;i<n;i++)
    {
       do
       {
          scanf("%d",&arreglo[i]);
          
       }while(!rango_binario(arreglo[i]));
    }
    int i = 0;
    while(i<n)
    {
      if(arreglo[i]==1)
      {
         res = res + 3;
      }
      else 
      {
         res = res + 1;
      }
      
         i = i + k;
      
   }
      int resp = puntos - res ;

    printf("%i\n",resp);
   
    return 0;
}
/*Implementacion de funciones*/
bool rango_n(int n)
{
   if((n>=2)&&(n<=25))
   {
      return true;
   }
   return false;
}
bool rango_k(int k ,int n)
{
   if((k>=1)&&(k<=n))
   {
      return true;
   }
   return false;
}
bool rango_binario(int bin)
{
   if((bin==0)||(bin==1))
   {
      return true;
   }
   return false;
}