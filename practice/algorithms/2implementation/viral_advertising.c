/*Declaracion de librerias*/
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
/*Prototipos de funciones*/
bool rango1_50(int n);
/*Estructura principal*/
int main() {

    /* Enter your code here. Read input from STDIN. Print output to STDOUT */    
    int n;
    int like = 0;
    int personas = 5;
    do
    {
       scanf("%d",&n);
       
    }while(!rango1_50(n));
    
    for(int i=0;i<n;i++)
    {
        like = like + floor(personas/2);
        personas = floor(personas/2) * 3;
    }
    
    printf("%i\n",like);
    
    return 0;
}
/*Implementacion de funciones*/
bool rango1_50(int n)
{
   if((n>=1)&&(n<=50))
   {
      return true;
   }
   return false;
}
