/*
                                                Chocolate Feast(Fiesta de chocolate).
DOCUMENTACION:

A Little Bobby le encanta el chocolate, y frecuentemente va a su tienda favorita (5 & 10)
Penny Auntie, con N dólares para comprar chocolates.
Cada chocolate tiene un costo plano de dólares, y la tienda tiene una promoción donde:
te permiten el comercio de envolturas de chocolate M a cambio de 1 pieza de chocolate gratis.

Por ejemplo, si M = 2 y Bobby tiene N = 4 dólares que usa para comprar 4 chocolates a C = 1 dólar cada uno
puede comerciar con las 4 envolturas para comprar 2 chocolates más.
Ahora tiene dos envolturas más que puede cambiar por 1 chocolate más.
Debido a que sólo tiene una envoltura izquierda en este punto y 1 < M
sólo fue capaz de comer un total de 7 piezas de chocolate.

Dado, N, C y M para viajes a la tienda, ¿puede determinar cuántos chocolates Bobby come durante cada viaje?

FORMATO DE ENTRADA:

La primera línea contiene un entero, T, que indica el número de viajes que Bobby hace a la tienda.
Cada línea I de las T líneas subsiguientes contiene tres enteros separados por espacio que describen:
los respectivos valores N, C y M para uno de los viajes de Bobby a la tienda.

FORMATO DE SALIDA:

Para cada viaje a Penny tía, imprimir el número total de chocolates Bobby come en una nueva línea.

EXPLICACION:

* Bobby hace los siguientes 3 viajes a la tienda:

1  Él gasta sus 10 dólares en 5 chocolates a 2 dólares cada uno.
   Luego los come y cambia las 5 envolturas para obtener 1 chocolate más. 
   Imprimimos el número total de chocolates que comió, que es 6.



2  Él gasta sus 12 dólares en 3 chocolates en 4 dólares cada uno;
   Sin embargo, él necesita 4 envolturas al comercio para su chocolate siguiente.
   Debido a que sólo tiene 3 envolturas, no puede comprar ni comerciar con más chocolates.
   Imprimimos el número total de chocolates que comió, que es 3.

3  Él gasta 6 dólares en 3 chocolates en 2 dólares cada uno.
   A continuación, intercambia 2 de las 3 envolturas por 1 pieza adicional de chocolate.
   A continuación, utiliza su tercera envoltura de chocolate sobrante de su compra inicial
   con la envoltura de su trade-in para hacer un segundo trade-in por 1 pieza más de chocolate.
   En este punto tiene 1 envoltorio a la izquierda, que no es suficiente para realizar otro trade-in.
   Imprimimos el número total de chocolates que comió, que es 5.


*/
// Declaracion de librerias.
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
// Funcion Principal.
int main(){
    // Declaracion de variable entera t: que denota el numero de viajes.
    int t; 
    // Ingresando dato t.
    scanf("%d",&t);
    // Declaracion de variables enteras chocolates, envolturas, envolturas no usadas.
    int chocolates, envolturas, envolturas_nousadas;
    // Iteramos t veces.
    for(int i = 0; i < t; i++){
        // Declaracion de variable entera n,c,m : n (dinero), c(precio), m (cantidad de envolturas para la promocion).
        int n,c,m; 
        // Ingresando datos n,c,m.
        scanf("%d %d %d",&n,&c,&m);
        // La cantidad de chocolates sera  dinero / precio.
        chocolates = n/c;
        // Envolturas es igual a la cantidad de chocolates.
        envolturas = chocolates;
        
        while(envolturas/m){
            // Chocolates = chocolates + envolturas / m.
            chocolates += envolturas/m;
            // Envolturas no usadas = envolturas % m.
            envolturas_nousadas = envolturas % m;
            // Envolturas = Envolturas / m.
            envolturas /= m;
            // Envolturas = Envolturas + Envolturas no usadas.
            envolturas += envolturas_nousadas;
        }
        // Imprimiendo la cantidad de chocolates.
        printf("%d\n", chocolates);
    }
    // Terminando ejecucion del programa con exito.
    return 0;
}