/*Declaracion de librerias*/
#include<stdio.h>
#include<stdbool.h>
/*Prototipo de funciones*/
bool rango1_100(int n);
int frecuencia(int arreglo[],int longitud);
int pos_elem_frec(int arreglo[],int longitud);
/*Estructura principal*/
int main (void)
{
   int n;
   int contador = 0;
   do
   {
      scanf("%d",&n);
   }while(!(rango1_100(n)));
   int arreglo[n];
   for(int i=0;i<n;i++)
   {
      do
      {
         scanf("%d",&arreglo[i]);
      }while(!(rango1_100(arreglo[i])));
   }
   int elemento = frecuencia(arreglo,n);
   for(int i=0;i<n;i++)
   {
      if(elemento!=arreglo[i])
      {
         contador = contador + 1;
      }
   }
   printf("%i\n",contador);
   
}
/*Inmplementacion de funciones */
bool rango1_100(int n)
{
   if((n>=1)&&(n<=100))
   {
      return true;
   }
   return false;
}

int frecuencia(int arreglo[],int longitud)
{
   /*Nuevo arreglo de tamaño 101*/
   int array[101];
   for(int i=0;i<101;i++)
   {
      array[i] = 0;
   }
   /*Iterando el arreglo dado*/
   for(int i=0;i<longitud;i++)
   {
      array[arreglo[i]] = array[arreglo[i]] + 1; 
   }
   int elemento = pos_elem_frec(array,101);
   
   return elemento;
   
}
int pos_elem_frec(int arreglo[],int longitud)
{
   int pos = 1;
   int mayor = arreglo[1];
   for(int i=1;i<longitud;i++)
   {
      if(arreglo[i]>mayor)
      {
         mayor = arreglo[i];
         pos = i;
      }
   }
      return pos;
}