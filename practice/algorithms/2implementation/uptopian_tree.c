/*Declaracion de librerias*/
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
/*Declaracion de funciones*/
bool rango1_10(int n);
bool rango0_60(int n);
int crecimiento(int n);
/*Estructura principal*/
int main(void){
    int t;
    do
    {
    scanf("%d",&t);
    }while(!(rango1_10(t)));
    int arreglo[t];
    for(int i = 0; i < t; i++)
    {
        do
        {
        scanf("%d",&arreglo[i]);
        }while(!(rango0_60(arreglo[i])));
           
    }
    
    for(int i=0;i<t;i++)
    {
      printf("%i\n",crecimiento(arreglo[i]));

    }
   
    return 0;
    
}

/*Implementacion de funciones*/
bool rango1_10(int n)
{
   if((n>=1)&&(n<=10))
   {
      return true;
   }
   return false;
}
bool rango0_60(int n)
{
   if((n>=0)&&(n<=60))
   {
      return true;
   }
   return false;
}

int crecimiento(int n)
{
   int flag =0;
   int altura = 1;
   for(int i=0;i<n;i++)
   {
      if(flag==0)
      {
         altura = altura * 2;
         flag = 1;
      }
      else if(flag==1)
      {
         altura = altura + 1;
         flag = 0;
      }
      
   }
   return altura;
}