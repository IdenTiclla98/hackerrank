/*
                                       Jumping on the Clouds (Saltando en las nubes).
DOCUMENTACION:
Emma está jugando un nuevo juego móvil involucrando N nubes numeradas de 0 a N - 1.
Un jugador inicialmente comienza en la nube C0, y deben saltar a la nube C (N-1).
En cada paso, puede saltar de cualquier nube i a nube i + 1 o nube i + 2.

Hay dos tipos de nubes, nubes ordinarias y nubes de tormenta.
El juego termina de Emma salta sobre una nube de tormenta, pero si ella llega a la última nube (es decir, c (n-1))
, ella gana el juego!

¿Puedes encontrar el número mínimo de saltos que Emma debe hacer para ganar el juego?
Se garantiza que las nubes c0 y c (n-1) son nubes ordinarias y siempre es posible ganar el juego.

FORMATO DE ENTRADA
* La primera línea contiene un número entero, N (el número total de nubes).
* La segunda línea contiene N números enteros binarios separados por el espacio que describen las nubes:
  c0, c1, .... c (n - 1).
  
   Si c_i = 0, la i-ésima nube es una nube ordinaria.
   Si c_i = 1, la i-ésima nube es una nube de tormenta.

FORMATO DE SALIDA:
* Imprime el número mínimo de saltos necesarios para ganar el juego.

*/
// Declaracion de librerias.
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
// Funcion Principal.
int main(){
    // Declaracion de variable entera n:que denota el numero total de nubes.
    int n; 
    // Ingresando dato n.
    scanf("%d",&n);
    // Declaracion de puntero a entero c con memoria dinamica para almacenar n enteros.
    int *c = malloc(sizeof(int) * n);
    // Iterando n veces.
    for(int c_i = 0; c_i < n; c_i++){
       // Ingresando nube iesima.
       scanf("%d",&c[c_i]);
    }
    // Declaracion de variable saltos inicializada en 0.
    int saltos = 0;
    // Declaracion de variable entera i de tipo entero.
    int i = 0;
    // Mientras i es menor a n - 1 Hacer:
    while(i < n - 1)
    {
       // Si el caracter en la posicion i + 2 es igual a cero. 
       if(c[i + 2] == 0){
          // Entonces damos 1 salto.
          saltos++;
          // El indice cambiara a i + 2.
          i = i + 2;
       }
       // Si no Si el caracter en la posicion i + 1 es igual a cero.
       else if(c[i + 1] == 0){
          // Entonces damos 1 salto.
          saltos++;
          // El indice cambiara a i + 1.
          i = i + 1;
       }
    }
    // Imprimimos la cantidad de saltos totales que se dio.
    printf("%d\n",saltos);
    // Liberando memoria dinamica prestada.
    free(c);
    // Terminar ejecucion con exito.
    return 0;
}
