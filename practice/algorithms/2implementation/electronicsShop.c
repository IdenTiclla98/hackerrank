/*
                                             Electronic shop (Tienda de electrónicos)
   DOCUMENTACION:
   
   Monica quiere comprar exactamente un teclado y una unidad USB de su tienda de electrónica favorita.
   La tienda vende (n) diferentes marcas de teclados y (m) diferentes marcas de unidades USB.
   Monica tiene exactamente (s) dólares para gastar, y quiere gastar tanto como sea posible 
   (es decir, el costo total de su compra debe ser máximo).
   
   Teniendo en cuenta las listas de precios de los teclados de la tienda y las unidades USB
   busque e imprima la cantidad de dinero que Monica gastará.
   Si no tiene suficiente dinero para comprar un teclado y una unidad USB, imprima -1 en su lugar.
   
   NOTA: Nunca comprará más de un teclado y una unidad USB, incluso si tiene el dinero sobrante para hacerlo.
   
   FORMATO DE ENTRADA:

   *La primera línea contiene tres enteros separados por espacio que describen los valores respectivos de (s)
   (la cantidad de dinero que tiene Monica), (n) (el número de marcas de teclado) y (m) (el número de marcas de unidad USB).
   *La segunda línea contiene (n) enteros separados por el espacio que indican los precios de cada marca de teclado.
   *La tercera línea contiene (m) enteros separados por el espacio que indican los precios de cada marca de unidad USB.
   
   FORMATO DE SALIDA:
   
   Imprima un solo entero que indique la cantidad de dinero que Monica gastará.
   Si no tiene suficiente dinero para comprar un teclado y una unidad USB, imprima -1 en su lugar.
*/

// Declaracion de librerias.
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>

// Prototipos de funciones
int getMoneySpent(int* keyboards, int* drives, int s, int *n, int *m);

// Funcion Principal.
int main()
{
    // Declaracion de varible entera s.
    int s; 
    // Declaracion de variable entera n.
    int n; 
    // Declaracion de variable entera m.
    int m; 
    // Ingresando datos s,n,m respectivamente.
    scanf("%d %d %d", &s, &n, &m);
    // Variable keyboards tipo puntero a entero con memoria dinamica para n enteros.
    int *keyboards = malloc(sizeof(int) * n);
    // Iteracion de la variable n desde keyboard_i = 0 hasta n - 1.
    for(int keyboards_i = 0; keyboards_i < n; keyboards_i++)
    {
       // Ingresando keyboards iesimo en el arreglo dinamico keyboards
       scanf("%d",&keyboards[keyboards_i]);
    }
    // Variable drives tipo puntero a entero con memoria dinamica para m enteros.
    int *drives = malloc(sizeof(int) * m);
    // Iteracion de la variable n desde drives_i = 0 hasta n - 1.
    for(int drives_i = 0; drives_i < m; drives_i++)
    {
       // Ingresando drives iesimo en el arreglo dinamico drives
       scanf("%d",&drives[drives_i]);
    }
    //  The maximum amount of money she can spend on a keyboard and USB drive, or -1 if she can't purchase both items
    int moneySpent = getMoneySpent(keyboards, drives, s,&n,&m);
    // Imprimiendo el gasto.
    printf("%d\n", moneySpent);
    // Terminar ejecucion con exito.
    return 0;
}


// Definicion de funcion getMoneySpent:
// Recibe como parametros un puntero a entero (KEYBOARDS), un puntero a los dispositivos(USB), un entero (s)
int getMoneySpent(int* keyboards, int* drives, int s, int *n, int *m)
{
    // Declaracion de variable entera gastoMayor que obtendra valor de un gasto mayor  no  mayor a s (el dinero que se tiene para gastar)
    int gastoMayor = -1;
    // Iteramos el valor que contiene n desde i = 0 hasta *n -1
    for(int i = 0; i < *n; i++)
    {
        // Iteramos el valor que contiene m desde i = 0 hasta *m -1
        for(int j = 0; j < *m; j++)
        {
            // Declaracion de gasto que sera los primeros keyboards y drives
            int gasto = keyboards[i] + drives[j];
            if(gasto > gastoMayor && gasto <= s)
            {
                gastoMayor = gasto;
            }
        }
    }
   return gastoMayor;

}