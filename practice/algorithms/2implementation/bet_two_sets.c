/*Declaracion de librerias*/
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
/*Declaracion de funciones que reciben un valor tipo entero y devulve true si esta adentro y false si no esta*/
bool rango1_10(int a);
bool rango1_100(int a);
bool factor_de_a(int arreglo[],int n,int x);
bool conjunto_fact_de_x(int arreglo[],int m,int x);
int main(void){
    int n; 
    int m;
    int contador = 0;
    do
    {
    scanf("%d %d",&n,&m);
    }while(!(rango1_10(n))||!(rango1_10(m)));
    /*int *a = malloc(sizeof(int) * n);
    for(int a_i = 0; a_i < n; a_i++){
       scanf("%d",&a[a_i]);
    }
    int *b = malloc(sizeof(int) * m);
    for(int b_i = 0; b_i < m; b_i++){
       scanf("%d",&b[b_i]);
    }*/
    /*Declaracion de arreglo1 y arreglo2 de tipo enteros con sus respectivos tamosños n y m*/
    int arreglo1[n];
    int arreglo2[m];
    /*Rellenando el arreglo1  con valores de 1 a100*/
    for(int i=0;i<n;i++)
    {
       do
       {
          scanf("%d",&arreglo1[i]);
       }while(!(rango1_100(arreglo1[i])));
    }
    /*Rellenando el arreglo2 con valores de 1 a 100*/
    for(int i=0;i<m;i++)
    {
       do
       {
          scanf("%d",&arreglo2[i]);
       }while(!(rango1_100(arreglo2[i])));
    }
    int inicio = arreglo1[n-1];
    int fin = arreglo2[0];
    for(int i=inicio;i<=fin;i++)
    {
       if((factor_de_a(arreglo1,n,i))&&(conjunto_fact_de_x(arreglo2,m,i)))
       {
           contador = contador + 1;
       }
    }
    
    printf("%i\n",contador);
    
    
    
    
    
    
    return 0;
}

/*Implementacion de funciones que retornan un valor booleano*/
bool rango1_10(int a)
{
   if((a>=1)&&(a<=10))
   {
      return true;
   }
   return false;
}
bool rango1_100(int a)
{
   if((a>=1)&&(a<=100))
   {
      return true;
   }
   return false;
}
bool factor_de_a(int arreglo[],int n,int x)
{
   for(int i=0;i<n;i++)
   {
       if(x % arreglo[i]!=0)
       {
           return false;
       }
       
   }
   return true;
}
bool conjunto_fact_de_x(int arreglo[],int m,int x)
{
    for(int i=0;i<m;i++)
    {
        if(arreglo[i] % x!=0)
        {
            return false;
        }
    }
    return true;
}