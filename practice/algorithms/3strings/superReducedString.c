/*                                                 Super Reduced String
DOCUMENTACION:
Steve tiene una cadena, s, que consta de n letras minúsculas alfabéticas en inglés.
En una operación, puede eliminar cualquier par de letras adyacentes con el mismo valor.
Por ejemplo, la cadena "aabcc" se convertiría en "aab" o "bcc" después de 1 operación.

Steve quiere reducir s tanto como sea posible. Para ello, repetirá la operación anterior tantas veces como se pueda realizar.
¡Ayuda a Steve a encontrar e imprimir la forma no reducible de s!

Nota: Si la cadena final está vacía, imprima Empty String.

FORMATO DE ENTRADA:
 * Una sola cadena, s.


FORMATO DE SALIDA:
 * Si la cadena final está vacía, imprima Empty String; De lo contrario, imprima la cadena final no reducible.


*/
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>

int charToIndex(char c);

int main() {

    char* cadena = malloc(sizeof(char)*1000);
    scanf("%[^\n]",cadena);
    char* buffer = malloc(sizeof(char)*1000);
    int length = strlen(cadena);
    
    int abc[26] = {0};
    
    
    for(int i = 0; i < length; i++){
      // Si la cadena-iesima es minuscula:
      if(islower(cadena[i])){
         // Incrementamos el indice correspondiente en el arreglo abc.
         abc[charToIndex(cadena[i])]++;   
      }
    }
    
    int j = 0;
    for(int i = 0; i < 26; i++){
      if(abc[i] % 2 != 0){
          buffer[j] = (char)(i + (int)'a');
          j++;
      }
    }    
    if (strlen(buffer) > 0)
    {
        printf("%s\n",buffer);
    }
    else{
        printf("Empty String\n");
    }
    free(buffer);
    free(cadena);
    
    return 0;
}

int charToIndex(char c){
   // Retornamos la diferencia entre el caracter - 'a' cuyos valores se restan en ascii.
   return (int)c - (int)'a';   
}

