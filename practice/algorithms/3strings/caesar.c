/*                                                    cifrado cesar
   DOCUMENTACION:
   
   Julio César protegió su información confidencial cifrándola en un cifrado.
   La cifra de César giraba cada letra en una cuerda por un número fijo, K, haciéndola ilegible por sus enemigos.
   Dada una cadena, S, y un número, K, cifran S e imprimen la cadena resultante.
   
   * La primera línea contiene un entero, n, que es la longitud de la cadena no cifrada.
   * La segunda línea contiene la cadena sin cifrar, s
   * La tercera línea contiene la clave de cifrado entero, k, que es el número de letra a rotar
   
   
*/
#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//prototypes
bool rango1(int a);
//Funcion principal
int main (void)
{
   //Declaracion de variables n,k
   int n,k;
   //Bucle hacer-mientras (do-while)
   do
   {
      //ingresando valor de n
      scanf("%d",&n);
   //Repetir mientras n no este en el rango
   }while(!rango1(n));
   //Declaracion de variable puntero s apuntara a un caracter
   char* s = malloc(n*sizeof(char));
   //Ingresando cadena
   scanf("%s",s);
   //Bucle hacer-mientras (do-while)
   do
   {
      //ingresando valor de k
      scanf("%d",&k);
      if(k<=0)
      {
         printf("%s\n",s);
         return 0;
      }
   //Repetir mientras k no este en el rango
   }while(!rango1(k));
   
   for(int i=0,m=strlen(s);i<m;i++)
   {
      if(isupper(s[i]))
      {
          //Si el caracter iesimo es mayuscula aplicamos la formula para mayuscula
         //          c_i = (p_i + k) % 26
         printf("%c",((s[i]-'A' + k) % 26) + 'A');
      }
      else if(islower(s[i]))
      {
         //Si el caracter iesimo es minuscula aplicamos la formula para minuscula
         //          c_i = (p_i + k) % 26
         printf("%c",((s[i]-'a' + k) % 26) + 'a');
      }
      else
      {
         //Caso contrario que no sea mayuscula ni minuscula, solo imprimos el caracter
         printf("%c",s[i]);
      }
   }
   //imprimir un salto de linea
   printf("\n");
   //Terminar la ejecucion del programa sin error
   return 0;
}
//Definition
bool rango1(int a)
{
   if((a>=1)&&(a<=100))
   {
      return true;
   }
   return false;
}
