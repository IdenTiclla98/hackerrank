/*                                                   CAMEL CASE.
   DOCUMENTACION:
   
   Alice escribió una secuencia de palabras en CamelCase como una cadena de letras, S, con las siguientes propiedades:
      * Es una concatenación de una o más palabras que consisten en letras en inglés.
      * Todas las letras de la primera palabra son minúsculas.
      * Para cada una de las palabras siguientes, la primera letra es mayúscula y el resto de las letras están en minúsculas.

   Dado S, imprime el número de palabras en S en una nueva línea.
   
   FORMATO DE ENTRADA:
      * Una sola línea que contiene la cadena S.
   
   FORMATO DE SALIDA:
      * Imprimir el número de palabras en la cadena S.
*/
// Declaracion de librerias.
#include <math.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
// Funcion Principal
int main()
{
    // Declaracion de puntero a caracter s (cadena) que obtendra memoria dinamica para 100240 caracteres.
    char* s = (char *)malloc(100240 * sizeof(char));
    // Obteniendo cadena.
    scanf("%s",s);
    // Convertimos a mayuscula el primer caracter.
    s[0] = toupper(s[0]);
    // Declaracion de contador de palabras tipo long
    long contPal = 0;
    for(long i = 0,longitud = strlen(s); i < longitud; i++)
    {
       // si el caracter iesimo de la cadena es mayuscula.
       if(isupper(s[i]))
       {
          // Incrementamos contador.
          contPal++;
       }
    }
    // Imprimimos la variable contador de palabras.
    printf("%ld\n",contPal);
    // Liberar memoria dinamica.
    free(s);
    //Terminar ejecucion con exito.
    return 0;
}
