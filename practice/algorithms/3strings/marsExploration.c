/*
                                                Mars Exploration (Exploración de Marte)
DOCUMENTACION:

¡La nave espacial de Sami se estrelló en Marte! Envía n mensajes secuenciales SOS a la Tierra para obtener ayuda.
Las letras en algunos de los mensajes SOS son alteradas por radiación cósmica durante la transmisión.
Dada la señal recibida por la Tierra como una cadena, S:
determine cuántas letras de SOS de Sami han sido cambiadas por radiación.

FORMATO DE ENTRADA:
* Hay una línea de entrada: una sola cadena, S.

Nota: Como el mensaje original es sólo SOS repetido n veces, la longitud de S será un múltiplo de 3.
      S sólo contendrá letras mayúsculas en inglés.
      
FORMATO DE SALIDA:
* Imprimir el número de letras en el mensaje de Sami que fueron alterados por la radiación cósmica.

*/

// Declaracion de librerias.
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
// Definicion de constante SOS igual a la cadena "SOS"
#define SOS "SOS"
// Funcion Principal.
int main(){
// Delcaracion de variable s tipo: puntero a caracter con memoria dinamica para 10240 caracteres. 
char* cadena = (char *)malloc(10240 * sizeof(char));
// Ingresando dato S.
scanf("%s",cadena);
// Calculando longitud de cadena Ingresada.
int length = strlen(cadena);
// Declaracion de puntero a caracter sos que sera el mensaje esperado secuencia de mensaje  sos.
char* sos = malloc(sizeof(char)*length);
// Declaracion de variable contador inicializado en 0.
int contador = 0;
// Declaracion de entero j iniciado en 0.
int j = 0;
// Iterando logitud veces
for(int i = 0; i < length; i++){
   // El caracter iesimo de sos igual al caracter j esimo de SOS
   sos[i] = SOS[j];
   // Si j es igual a 2.
   if(j == 2){
      // Volvemos el indice a cero.
      j = 0;
   }
   //Por falso
   else{
      //Incrementamos j.
      j++;
   }
}
// Iterando la longitud.
for(int i = 0; i < length; i++){
   // Si el caracter iesimo de sos es diferente al iesimo de la cadena.
   if(sos[i] != cadena[i]){
      //Incrementamos contador.
      contador++;
   }
}
// Imprimimos el contador.
printf("%d\n",contador);
// Liberando memoria que nos prestamos para cadena.
free(cadena);
// Liberando memoria que nos prestamos para sos.
free(sos);
// Terminar ejecucion del programa con exito.
return 0;
}