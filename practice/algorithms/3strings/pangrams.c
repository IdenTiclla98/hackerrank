/*
                                                      Pangrams
DOCUMENTACION:

Roy quiere mejorar su velocidad de escritura en máquina para concursos de programación.
Su amigo le dijo que escribiera la oración:
"The quick brown fox jumps over the lazy dog" repetidamente porque es un pangrama.
(pangramas son oraciones construidas usando todas las letras del alfabeto, por lo menos una vez.)
Después de escribir la oración muchas veces, Roy se aburrió. Entonces comenzó a buscar otros pangramas.
Dada una oración s, dile a Roy si es un pangrama o no.

FORMATO DE ENTRADA:
 * La Entrada consiste en una linea que contiene s.

Formato de Salida
 *Imprime una línea que contiene pangram si s es un pangrama, sino imprime not pangram.

*/

// Declaracion de librerias.
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdbool.h>
// Declaracion de constante LIMI con valor 1001
#define LIMIT 1001
#define LIM_ABC 26
// Prototipo de funciones.
char* converMinus(char* string);
int charToIndex(char c);
bool isPangram(int abc[]);
// Funcion Pricipal.
int main(){
   // Declaracion de puntero a caracter cadena con memoria dinamica para LIMIT caracteres.
   char* cadena = malloc(sizeof(char) * LIMIT);
   // Ingresando cadena.
   scanf("%[^\n]",cadena);
   // La cadena la convertimos a minusculas.
   cadena = converMinus(cadena);
   // Declaracion de variable length que tendra el tamaño de la cadena.
   int length = strlen(cadena);
   // Declaracion de arreglo de enteros abc con dimension para 26 enteros. 
   int abc[26]={0};
   // Iteramos la longitud.
   for(int i = 0; i < length; i++){
      // Si la cadena-iesima es minuscula:
      if(islower(cadena[i])){
         // Incrementamos el indice correspondiente en el arreglo abc.
         abc[charToIndex(cadena[i])]++;   
      }
   }
   // Si el arreglo cumple las condiciones para ser Pagram:
   if(isPangram(abc)){
      // Imprimir pangram.
      printf("pangram\n");
   }
   // Por falso.
   else{
      //Imprimir not pangram.
      printf("not pangram\n");
   }
   // Terminar ejecucion con exito.
   return 0;
}
// Definicion de funciones.

char* converMinus(char* string){
   // length agarra la longitud de string.
   int length = strlen(string);
   // Iteramos la longitud.
   for(int i = 0;i < length; i++){
      // Convertimos a minuscula el caracters iesimo de string-iesimo.
      string[i] = tolower(string[i]);
   }
   // Retornamos la cadena.
   return string;
}
int charToIndex(char c){
   // Retornamos la diferencia entre el caracter - 'a' cuyos valores se restan en ascii.
   return (int)c - (int)'a';   
}
bool isPangram(int abc[]){
   // Itermaos la longitud del abecedario.
   for(int i = 0; i < LIM_ABC; i++){
      // Si algun elemento del arreglo es 0.
      if(abc[i] == 0){
         // Retornamos falso.
         return false;
      }
   }
   // Caso contrario entonces es verdadero.
   return true;
}