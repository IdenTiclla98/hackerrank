/*
                                                      Hackerrank in a string.
DOCUMENTACION:

Decimos que una cadena, S, contiene la palabra hackerrank si una subsecuencia de los caracteres en S deletrea la palabra hackerrank.
Por ejemplo:
 * haacckkerrannkk contiene hackerrank
 * pero haacckkerannk no (todos los caracteres aparecen en el mismo orden, pero falta un segundo r).

Más formalmente, sean p0..p1..p2..p9 los respectivos índices de h, a, c, k, e, r, r, a, n, k en la cadena S.
Si p0 <p1 <p2 .. <P9 es true, entonces S contiene hackerrank.

Debe responder a q consultas, donde cada consulta i consiste en una cadena, s_i.
Para cada consulta, imprima SI en una nueva línea si s_i contiene hackerrank;
De lo contrario, imprima NO en su lugar.


FORMATO DE ENTRADA:

* La primera línea contiene un entero que denota q (el número de consultas).
* Cada línea i de las q líneas subsiguientes contiene una sola cadena que denota s_i.

FORMATO DE SALIDA:

* Para cada consulta, imprima SI en una nueva línea si s_i contiene hackerrank; De lo contrario, imprima NO en su lugar.
*/
// Declaracion de librerias.
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define HACKERRANK "hackerrank"
// Funcion Principal.
int main()
{
 // Declaracion de variable entera q: denota el numero de casos.
 int q; 
 // Ingresando q.
 scanf("%d",&q);
 int long_h = strlen(HACKERRANK);
 
 // Iterando q veces.
 for(int i = 0; i < q; i++)
 {
     // Declaracion de puntero a caracter s (cadena) con memoria dinamica para 512000 caracteres.
     char* s = (char *)malloc(512000 * sizeof(char)); // No entiendo para que sirve (char *). 
     // Ingresando cadena s.
     scanf("%s",s);
     // your code goes here
     int long_s = strlen(s);
     int k = 0;
     // Si la longitud de hackerrank y s son iguales && hackerrank es igual a s.
     if(long_h == long_s && strcmp(HACKERRANK,s) == 0)
     {
        // Imprimimos YES.
        printf("YES\n");
     }
     // Por falso    
     else 
     {
        // Declaracion de variable j iniciada en 0.
        int j = 0;
        // Iterando la longitud de cadena s.
        for(int k = 0; k < strlen(s); k++)
        {
           // Si el caracter j-esimo de HACKERRANK es igual al caracter k-esimo de s. 
           if (HACKERRANK[j] == s[k]){
               // Por verdadero aumentamos el indice de busqueda de HACKERRANK
               j++;
           }
        }
        // Si la longitud de HACKERRANK es igual al indice de busqueda.
        if (long_h == j)
        {
           // Imprimimos YES.
            printf("YES\n");
        }
        // Por falso
        else
        {
            // Imprimimos NO.
            printf("NO\n");
        }
     }
     
 }
// Terminar ejecucion con exito.
return 0;
}
