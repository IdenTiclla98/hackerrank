/*Declarando librerias*/
#include<stdio.h>

/*Estructura principal*/
int main (void)
{
   /*Declarando variables y obteniendo la longitud*/
   int longitud;
   int suma = 0;
   scanf("%d",&longitud);
   
   /*Declaracion de arreglo tamaño de la longitud*/
   int array[longitud];
   
   /*Recorriendo todo el arreglo y obteniendo cada valor para guardarlo en su respectiva posicion*/
   for (int i=0;i<longitud;i++)
   {
      scanf("%d",&array[i]);
      suma = suma + array[i];
   }
   /*Mostrando suma de elementos*/
   printf("%d",suma);
}