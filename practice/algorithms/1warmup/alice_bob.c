/*Declarando librerias*/
#include<stdio.h>

/*Estructura principal*/
int main (void)
{
   /*Declaracion de variables*/
   int punto_alice = 0;
   int punto_bob = 0;
   
   int array_alice[3];
   int array_bob[3];
   /*Bucle for para obtener 3 elementos y guardarlos en array_alice de tipo enteros*/
   for(int i=0;i<3;i++)
   {
      scanf("%d",&array_alice[i]);
   }
   /*Bucle for para obtener 3 elementos y guardarlos en array_bob de tipo enteros*/
   for(int i=0;i<3;i++)
   {
      scanf("%d",&array_bob[i]);
   }
   /*Bucle for para comparar elementos de ambos array*/
   for(int i=0;i<3;i++)
   {
      /*Si el elemento de la posicion i de alice es mayor que el de bob alice gana un punto*/
      if(array_alice[i]>array_bob[i])
      {
         punto_alice = punto_alice + 1;
      }
      /*Por falso Si el elemento de la posicion i de alice es diferente que el de bob bob gana un punto*/
      else if(array_alice[i] != array_bob[i])
      {
         punto_bob = punto_bob + 1;
      }
   }
   
   printf("%i %i\n",punto_alice,punto_bob);
      
}