/*Declarando librerias*/
#include<stdio.h>

/*Estructura principal*/
int main (void)
{
   /*Declarando contadores de tipo real por que al 
   momento de dividir tienen que ser reales si fueran enteros
   perdemos valores*/
   float cont_pos = 0.0;
   float cont_cero = 0.0;
   float cont_neg = 0.0;
   int longitud;
   
   float fracc_pos;
   float fracc_cero;
   float fracc_neg;
   
   scanf("%d",&longitud);
   int array[longitud];
   
   for(int i=0;i<longitud;i++)
   {
      scanf("%d",&array[i]);
      if(array[i]>0)
      {
         cont_pos = cont_pos + 1;
      }
      else if(array[i]==0)
      {
         cont_cero = cont_cero + 1;
      }
      else if (array[i]<0)
      {
         cont_neg = cont_neg + 1;
      }
   }
   
   fracc_pos = cont_pos / longitud;
   fracc_cero = cont_cero / longitud;
   fracc_neg = cont_neg / longitud;
   
   printf("%.4f\n",fracc_pos);
   printf("%.4f\n",fracc_neg);
   printf("%.4f\n",fracc_cero);
   return 0;
}