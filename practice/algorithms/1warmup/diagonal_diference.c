/*Declarando librerias*/
#include<stdio.h>
#include <stdlib.h>
/*Estructura principal*/
int main (void)
{
    
    int longitud;
    scanf("%d",&longitud);
    int prim_diag = 0;
    int sec_diag = 0;
    int diference;
    int array[longitud][longitud];
    
    int k=0;
    /*Obteniendo valores para el array  multidimensionales*/
    for(int i=0;i<longitud;i++)
    {
        for(int j=0;j<longitud;j++)
        {
            scanf("%d",&array[i][j]);
        }
    }
    /*Sacando la suma de la diagonal principal*/
    for(int i=0;i<longitud;i++)
    {
        prim_diag = prim_diag + array[i][i];
    }
    /*Sacando la suma de la diagonal secundaria*/
    for(int i=(longitud - 1);i>=0;i--)
    {
        sec_diag = sec_diag + array[k][i];
        k = k + 1;
    }
    /*Calculando el valor absoluto de la resta entre la diagonal principal y la diagonal secundaria*/
    diference = abs(prim_diag-sec_diag);
    printf("%d\n",diference);
    /*Return 0  para indicar que no ocurrio un problema*/
    return 0;
}