/*Declaracion de librerias*/
#include<stdio.h>
#include<string.h>
/*Declaracion de procedimiento que imprime  la cantidad de caracteres que recibe*/
void imprimircaracter(int cantidad,char caracter);
/*Estructura principal*/
int main (void)
{
   int longitud;
   scanf("%d",&longitud);
   int cant_espacio = longitud -1;
   int cont = 1;
   for (int i=0;i<longitud;i++)
   {
         imprimircaracter(cant_espacio,' ');
         imprimircaracter(cont,'#');
         cant_espacio = cant_espacio - 1;
         cont = cont + 1;
      printf("\n");
   }
   
}
/*Implementacion de procedimiento*/
void imprimircaracter(int cantidad, char caracter)
{
   for(int i=0;i<cantidad;i++)
   {
      printf("%c",caracter);
   }
}