/*
                                                Birthday Cake Candle
   DOCUMENTACION                       
   
   PROBLEMA:
   
   *  ¡Colleen se está cumpliendo  n años! Ella tiene n velas de varias alturas en su pastel, y la vela i tiene altura height_i.
      Porque las velas más altas se elevan sobre las más pequeñas, Colleen sólo puede apagar las velas más altas.

   *  Dado el height_i para cada vela individual, encuentre e imprima el número de velas que puede soplar con éxito.
   
   FORMATO DE ENTRADA:

   *La primera línea contiene un solo número entero, que denota el número de velas en la torta. 

   *La segunda línea contiene números enteros separados por un espacio, donde cada número entero describe la altura de la vela .

   LINK: https://www.hackerrank.com/challenges/birthday-cake-candles
   
   EXPLICACION 0: 

   *Tenemos una vela de la altura 1, una vela de la altura 2, y dos velas de la altura 3.
    Colleen sopla solamente las velas más altas, significando las velas donde la altura = 3.
    Porque hay 2 tales velas, imprimimos 2 en una nueva línea.
*/
// Declaracion de Librerias
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
// Prototipos de funciones
bool rango_n(int n);
bool rango_altura(int altura);
// Funcion principal
int main ()
{
   // Declaracion de variable entera n que denotara el numero de velas
   int n;
   // Bucle Do-While (Hacer - Mientras)
   do
   {
      //Ingresando dato n
      scanf("%d",&n);
   }
   while(!rango_n(n)); // Mientras n no este en el rango

   // Arreglo de enteros alturas (dinamico) que tendra memoria para n enteros 
   int *alturas = malloc(sizeof(int) * n);
   
   // Iterando la cantidad de velas (n) desde i = 0  hasta (n - 1)
   for(int i = 0 ; i < n; i++)
   {
      // Bucle Do-While (Hacer - Mientras)
      do
      {
         // Ingresando altura_i;
         scanf("%d",(alturas+(i)));
      }
      while(!rango_altura(*(alturas+(i)))); // Mientras la altura_i no este en el rango
   }
   
   // Declaracion de variable mayor
   int mayor;
   // Iterando la variable n desde i = 0 hasta i = (n - 1)
   for(int i = 0; i < n;i++)
   {
      // Si estamos en la primera Iteracion
      if(i == 0)
      {
         // El mayor es el primer elemento del arreglo altura
         mayor = *(alturas+(i));
      }
      // Por falso 
      else
      {
         // Si el elemento iesimo del arreglo altura es mayor a la variable mayor
         if(*(alturas+(i)) > mayor)
         {
            // mayor sera ese elemento iesimo
            mayor = *(alturas+(i));
         }
      }
   }
   // Declaracion de variable tipo entero contador iniciada en 0;
   int contador = 0;
   // Iteramos la variable n desde i = 0 hasta i = (n - 1)
   for(int i = 0; i < n; i++)
   {
      // Si el elemento iesimo del arreglo altura es igual a la variable mayor
      if(*(alturas+(i)) == mayor)
      {
         // Incrementamos el contador
         contador++;
      }
   }
   // Impresion de la variable contador
   printf("%i\n",contador);
   // Liberando memoria dinamica 
   free (alturas);
   // Retornar 0 No hubo error alguno
   return 0;
}
// Definicion de Funciones
bool rango_n(int n)
{
   // Si (n es mayor o igual a 1) y (n es menor o igual a 10^5)
   if((n >= 1) && (n <= trunc(pow(10,5))))
   {
      // Si la condicion es verdad
      // Retornamos Verdadero
      return true;
   }
   // Caso contrario 
   // Retornar Falso
   return false;
}
bool rango_altura(int altura)
{
   // Si (la altura es mayor o igual a 1) y (la altura es menor o igual a 10^7)
   if((altura >= 1) && (altura <= trunc(pow(10,7))))
   {
      // Si la condicion es verdad
      // Retornamos Verdadero
      return true;
   }
   // Caso contrario 
   // Retornar Falso
   return false;
}