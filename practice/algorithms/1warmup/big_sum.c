/*Declarando librerias*/
#include <stdio.h>
#include <math.h>
/*Declaracion de funcion suma  que recibe dos parametros enteros grandes*/
long suma(long num1, long num2);
/*Declaracion de estructura principal*/
int main (void)
{
   /*Declarando variables tipo long*/
   long longitud;
   long resultado=0;
   /*Bucle while hacer-mientras   mientras la longitud ingresada es mayor o igual a 1 y menor igual a 10 
                     Niego toda la expresion por que el bucle while sale por falso
   */
   do
   {
      scanf("%ld",&longitud);   
   }
   while(!(longitud>=1) && (longitud <=10));
   
   /*Declaracion de arreglo tipo long*/
   long array[longitud];
   
   /*Con bucle for pido N datos para el array*/
   for(int i=0;i<longitud;i++)
   {
      do
      {
      scanf("%ld",&array[i]);
      }
      /*Mientras el dato del arreglo de la posicion i es mayor o igual a 0 y menor igual a 2^31
                        Bucle del while sale por falso
      */
      while(!(array[i]>=0) && (array[i]<=(long)pow(2,31)));
      resultado = suma(resultado,array[i]);
   }
   /*Mostrando la suma de todos los elementos del array*/
   printf("%ld\n",resultado);
}
long suma(long num1, long num2)
{
   return num1 + num2;
}