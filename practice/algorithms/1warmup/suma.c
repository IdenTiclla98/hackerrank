/*Declaracion de librerias*/
#include<stdio.h>
#include<stdlib.h>

/*declaracion de funcion suma que recibe dos parametros 
  de tipo enteros (int a, int b ) que a su vez devuelve un resultado tipo entero
*/
int suma(int a, int b);
/*Estructura principal del programa principal del programa*/
int main (void)
{  
   /*Declaracion de las variables*/
   int num1;
   int num2;
   /*pidiendo y obteniendo primera variable*/
   printf("Ingrese un numero\n");
   scanf("%d",&num1);
   /*pidiendo y obteniendo segunda variable*/
   printf("Ingrese un numero\n");
   scanf("%d",&num2);
   /*Mostrando los resultados*/
   printf("la suma es %i \n",suma(num1,num2));
   
   
   /*Para indicar  que no existen errores*/
   return 0;
}
/*Implementacion de la funcion suma*/
int suma(int a, int b)
{
   return a+b;
}